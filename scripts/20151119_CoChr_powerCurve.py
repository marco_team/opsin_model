# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 17:54:43 2015

@author: marco
"""

from marco_tools.experiment import *
import matplotlib.pyplot as pypl
import numpy as np
from marco_tools.parseDatafile import parseDatafile
import pickle

#%%
folder = "/home/marco/Desktop/_marco/ospin_model/experimental/CoChr_powerCurve"
params = parseDatafile(folder + '/' + 'parameters.dat' ,'\t')

expFolder = experimentalFolder(folder)

# populate experiment aray with the data in folder
exps = experimentCollection()
for it,filename in enumerate(sorted(expFolder.file_names_as_string_list)):
    e = opsinExperiment(0,1,folder,filename)
    e.set_current(step=200,mean_between_steps=True)
#   set light power    
    #temp_id = params[0].index(e.name)
    #e.set_ligth_power(params[1][temp_id])
    #e.set_ligth_power(params[1][temp_id])
    e.set_ligth_power(params[1][it])
#   Append to experiment collection    
    exps.append(e)

# remove wrong experiments based on STD
#for i,e in enumerate(exps):
#    if np.std(e.current) < 10:
#        print 'Eliminated ' + e.filename
#        exps.pop(i)

# removed by hand


exps.x_axis_domain = exps[0].current_time_space
exps.write_to_CSV(folder+'/'+'reduced_all.csv')

exps.pop_by_name('46')    
exps.pop_by_name('50')    

pickle.dump(exps, open(folder+'/'+"restricted_all.pickle",'wb'))

#%%
exps = pickle.load(open(folder+'/'+"restricted_all.pickle",'rb'))
#exps_selection = exps.get_selection(['51','49'])

# plot data     
for e in exps:
    pypl.plot( e.current_time_space, e.current, label=e.filename_prefix + ' : ' + e.light_power )
pypl.legend()

pypl.show()



