# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 09:54:30 2015

@author: marco
"""

from __future__ import division
from matplotlib.pyplot import *
from scipy import *
from scipy.integrate import odeint

#   O----
#           D----
#
#   C----
clf()   #clear figure
ion()   #makes plots interactives


koc = 1/20  #1/t_off in [ms]
kco = koc*10

kod = koc/3
kdo = koc/0.7

def caracteristicPolynome(x):
    """evalueate the caracteristical polinome of a 3 states system"""
#    return -(kco+x)*((ko+x)*(kdo+x)-kod*kdo)
    global p
    p = array([
        -1,
        (-kod-koc-kdo-kco),
        (-kco*kod-kdo*koc-kco*kdo),
        0
        ])
    
    # model with kod = 0
#    p = array([
#        -1,
#        (-kod-koc-kco),
#        (-kco*kod-kco*koc),
#        0
#        ])
    return dot(p,[x**3,x**2,x,1])
    
def plotWithRoots(func):
    z = roots(p)
#    print z
    
    x_min = min(z)-(abs(max(z) - min(z)/2))#-15
    x_max = max(z)+(abs(max(z) - min(z)/2))#120 # ms    
    x = linspace(x_min,x_max)
    y = array([func(elm) for elm in x])
    plot(x,y)
    plot(x,zeros(len(y)))
    
    for val in z:
        plot([val]*len(y),linspace(min(y),max(y),len(y)))
        
    axis([x_min,x_max,min(y),max(y)])


# autovalues of the semplified 3 level system

def autoval1(kdo):
    return -((sqrt(kod**2+(2*koc+2*kdo-2*kco)*kod+koc**2+(2*kco-2*kdo)*koc+kdo**2-2*kco*kdo+kco**2)+kod+koc+kdo+kco))/2
    
def autoval2(kdo):
    return (sqrt(kod**2+(2*koc+2*kdo-2*kco)*kod+koc**2+(2*kco-2*kdo)*koc+kdo**2-2*kco*kdo+kco**2)-kod-koc-kdo-kco)/2

def autoval3(kdo):
    return [0]*len(kdo)

#plotWithRoots(caracteristicPolynome)
#show()
def plotautAutoval():
    t = linspace(0,1,1000)
    plot(t,autoval1(t),'g',label='eigenval 1')
    plot(t,autoval2(t),'r',label='eigenval 2')
    plot(t,autoval3(t),'b',label='eigenval 3')
    legend()
    axis([0,0.8,-0.6,0.6])

caracteristicPolynome(1)
plotautAutoval()
figure(2)
clf()
plotWithRoots(caracteristicPolynome)
show()