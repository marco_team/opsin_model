# -*- coding: utf-8 -*-
from __future__ import division
from scipy import *
from pylab import *
from scipy.integrate import odeint      # Module de résolution des équations différentielles
import matplotlib.pyplot as plt
from matplotlib.widgets import RadioButtons,Slider


###############################################################################
######################### ESPACE DE LA FIGURE PRINCIPAL #######################
###############################################################################

fig, ax = plt.subplots()
plt.subplots_adjust(left=0.3, bottom=0.45,top=0.95)

###############################################################################
####################### DECLARATION DES DEFINITIONS ###########################
###############################################################################

def func(y,t):

    eclairement_carre=0
    for i in range(int(floor(Npulses)-1)):
        if i*Dpulses<t<=i*Dpulses+tpulse:
            eclairement_carre=1
        
    
    #eclairement_carre = around(0.5+sin(coefft*pi*t/100.0)/2.0)

    etat_1 = (1-y[0]-y[1])/t_reconstitution+y[1]/t_off-eclairement_carre*y[0]/tg
    etat_2 = eclairement_carre*y[0]/tg-y[1]/t_off-y[1]/t_desactivation
    etat_3 = -(1-y[0]-y[1])/t_reconstitution+y[1]/t_off+y[1]/t_desactivation
	
	
    #eclairement_gaussien = 1/(demi_long*sqrt(2*np.pi))*exp(-0.5*((t-m1)/demi_long)**2)+1/(demi_long*sqrt(2*np.pi))*exp(-0.5*((t-m2)/demi_long)**2)
        
    #etat_1 = A20*(1-y[0]-y[1])+A10*y[1]-B01*eclairement_gaussien*y[0]
    #etat_2 = B01*eclairement_gaussien*y[0]-A10*y[1]-A12*y[1]
    #etat_3 = -A20*(1-y[0]-y[1])+A10*y[1]+A12*y[1]
	
    return array([etat_1,etat_2,etat_3])

def func2(y,t):

    eclairement_carre=0
    for i in range(int(floor(Npulses)-1)):
        if i*Dpulses<t<=i*Dpulses+tpulse:
            eclairement_carre=1
        
    
    #eclairement_carre = around(0.5+sin(coefft*pi*t/100.0)/2.0)

    etat_12 = (1-y[0]-y[1])/t_reconstitution2+y[1]/t_off2-eclairement_carre*y[0]/tg2
    etat_22 = eclairement_carre*y[0]/tg2-y[1]/t_off2-y[1]/t_desactivation2
    etat_32 = -(1-y[0]-y[1])/t_reconstitution2+y[1]/t_off2+y[1]/t_desactivation2
	
	
    #eclairement_gaussien = 1/(demi_long*sqrt(2*np.pi))*exp(-0.5*((t-m1)/demi_long)**2)+1/(demi_long*sqrt(2*np.pi))*exp(-0.5*((t-m2)/demi_long)**2)
        
    #etat_1 = A20*(1-y[0]-y[1])+A10*y[1]-B01*eclairement_gaussien*y[0]
    #etat_2 = B01*eclairement_gaussien*y[0]-A10*y[1]-A12*y[1]
    #etat_3 = -A20*(1-y[0]-y[1])+A10*y[1]+A12*y[1]
	
    return array([etat_12,etat_22,etat_32])

def update(val):
    global t_desactivation
    t_desactivation = sA12.val
    global t_off
    t_off = sA10.val
    global t_reconstitution
    t_reconstitution = sA20.val
    global tg
    tg = sB01.val

    global t_desactivation2
    t_desactivation2 = sA122.val
    global t_off2
    t_off2 = sA102.val
    global t_reconstitution2
    t_reconstitution2 = sA202.val
    global tg2
    tg2 = sB012.val


    global tpulse
    tpulse = stpulse.val
    global freq
    freq = saxfreq.val
    global Npulses
    Npulses = around(saxNpulses.val)
    global Dpulses
    Dpulses=1/freq*1000; #ms 

    m.set_ydata(odeint(func,y0,t)[:,1])
    m2.set_ydata(odeint(func2,y02,t)[:,1])

    fig.canvas.draw_idle()
    
def reset(event):
    sB01.reset()
    sA12.reset()
    sA10.reset()
    sA20.reset()

    stpulse.reset()
    saxfreq.reset()
    saxNpulses.reset()

# 2nd opsins
    sB012.reset()
    sA122.reset()
    sA102.reset()
    sA202.reset()
################################################################################
####################### DECLARATION DES CONSTANTES #############################
################################################################################

A12 =  1/75   
t_desactivation = 1./A12
A10 = 1/50.
t_off = 1./A10
B01 = 1/6.51
tg=1./B01
#B01s  = linspace(0,5,5)
A20 =1/53.39
t_reconstitution = 1./A20

A122 =  1./50.    
t_desactivation2 = 1./A122
A102 = 1./25.
t_off2 = 1./A102
B012 =1./4.43 
tg2=1./B012
#B01s  = linspace(0,5,5)

A202 = 1./95    
t_reconstitution2 = 1./A202

tpulse=10 #ms
freq=50 #Hz
Npulses=5
Dpulses=1/freq*1000; #ms 

demi_long=0.4
m1 = 1.25
m2 = 6.25

start = 0
end = 500
numsteps = 1000

###############################################################################
################ CONDITION INITIALE ET RESOLUTION DE L'EQUA DIFF ##############
###############################################################################

y0=array([1.,0.])
y02=array([1.,0.])
t = linspace(start,end,numsteps)
Imax = []
Iss = []

v2=odeint(func2,y0,t)

v=odeint(func,y0,t)

###############################################################################
############################# TRACER DES COURBES ##############################
###############################################################################

plt.axis([0, 500, -0.1, 1])

m2, = plt.plot(t,v2[:,1],lw=2, color='red')
m, = plt.plot(t,v[:,1],lw=2, color='blue')


plt.grid()

Imax.append(max(v[:,1]))
Iss.append((v[8,1]))


################################################################################
###########################CREATION DES SLIDERS ################################
################################################################################
axcolor = 'lightgoldenrodyellow'

axB01 = plt.axes([0.3, 0.27, 0.6, 0.02], axisbg=axcolor)
axA12 = plt.axes([0.3, 0.24, 0.6, 0.02], axisbg=axcolor)
axA10 = plt.axes([0.3, 0.21, 0.6, 0.02], axisbg=axcolor)
axA20 = plt.axes([0.3, 0.18, 0.6, 0.02], axisbg=axcolor)

axtpulse = plt.axes([0.3, 0.36, 0.6, 0.02], axisbg=axcolor)
axfreq = plt.axes([0.3, 0.30, 0.6, 0.02], axisbg=axcolor)
axNpulses = plt.axes([0.3, 0.33, 0.6, 0.02], axisbg=axcolor)

sB01 = Slider(axB01, 'tg', 0, 100 , valinit=tg)
sA12 = Slider(axA12, 't_desactivation', 0, 1000 , valinit=t_desactivation)
sA10 = Slider(axA10, 't_off', 0, 1000, valinit=t_off)
sA20 = Slider(axA20, 't_reconstitution', 0, 1000 , valinit=t_reconstitution)

stpulse = Slider(axtpulse, 't_pulse', 0, 300 , valinit=tpulse,color='green')
saxfreq = Slider(axfreq, 'frequency', 0, 100 , valinit=freq,color='green')
saxNpulses = Slider(axNpulses, 'Npulse', 0, 50, valinit=Npulses,color='green')

sB01.on_changed(update)
sA12.on_changed(update)
sA10.on_changed(update)
sA20.on_changed(update)

stpulse.on_changed(update)
saxfreq.on_changed(update)
saxNpulses.on_changed(update)



#second opsins
axB012 = plt.axes([0.3, 0.15, 0.6, 0.02], axisbg=axcolor)
axA122 = plt.axes([0.3, 0.12, 0.6, 0.02], axisbg=axcolor)
axA102 = plt.axes([0.3, 0.09, 0.6, 0.02], axisbg=axcolor)
axA202 = plt.axes([0.3, 0.06, 0.6, 0.02], axisbg=axcolor)

sB012 = Slider(axB012, 'tg_2', 0, 100 , valinit=tg2,color='red')
sA122 = Slider(axA122, 't_desactivation_2', 0, 100 , valinit=t_desactivation2,color='red')
sA102 = Slider(axA102, 't_off_2', 0, 100, valinit=t_off2,color='red')
sA202 = Slider(axA202, 't_reconstitution_2', 0, 150 , valinit=t_reconstitution2,color='red')



sB012.on_changed(update)
sA122.on_changed(update)
sA102.on_changed(update)
sA202.on_changed(update)



################################################################################
################################### RESET ######################################
################################################################################

#resetax = plt.axes([0.02, 0.9, 0.1, 0.04])
#button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
#button.on_clicked(reset)

plt.show()