# -*- coding: utf-8 -*-
"""
Created on Fri Sep 25 11:15:06 2015

@author: marco
"""

from matplotlib.pyplot import *
from scipy import *
from scipy.integrate import odeint

from api.parseDatafile import *


def two_states_simple_system(n,t):
    # costante di promozione dal livello zero al livello 1
    k01 = 1/0.2
    # costante di decadimento dal livello 1 al livello 0
    k10 = k01/2
    return [k10*n[1]-k01*n[0],-k10*n[1]+k01*n[0]]
    
    

def three_states_decay_simple_system(n,t):
    # state O is initially full and decays in D and C
    koc = 1/0.2
    kod = koc/2
    return [-(koc+kod)*n[0],koc*n[0],kod*n[0]]
    

def two_states_with_light(n,t):
    pulse_duration = 0.2
    if t < pulse_duration:
        k01 = 1/0.2
    else:
        k01 = 0
    k10 = 1/0.2
    return [k10*n[1]-k01*n[0],-k10*n[1]+k01*n[0]]
  

#def four_states_with_light(n,t):
#    # Is the transition light activated?
#    C1 = 0  # Closed state 1
#    O1 = 1  # Open state 1
#    O2 = 2  # Open state 2
#    C2 = 3  # Closed state 2
#    
#    # just renameing for better convenience
#    Nc1 = n[0]
#    No1 = n[1]
#    No2 = n[2]    
#    Nc2 = n[2]    
#    
#    pulse_duration = 0.5
#    
#    # transition coefficients when light is OFF
#    K_off = zeros([4,4])
#    # transition coefficients when light is ON
#    K_on =  zeros([4,4])
#
#    v = 1/0.05
#    
#    K_on[O1,C1] = v
#    K_off[O1,C1] = v
#    
#    K_on[C1,O1] = v*5
#    K_off[C1,O1] = 0
#
#    K_on[O1,O2] = v
#    K_off[O1,O2] = v
#    
#    K_off[O2,O1] = v
#    K_on[O2,O1] = v
#    
#    K_on[O2,C2] = v*0.1
#    K_off[O2,C2] = v*0.1 
#
#    K_on[C2,O2] = 0
#    K_off[C2,O2] = 0
#
#    K_on[C1,C2] = v
#    K_off[C1,C2] = v
#    
#    K_on[C2,C1] = 0
#    K_off[C2,C1] = 0
#    
#    if t < pulse_duration:
#        K = K_on
#    else:
#        K = K_off
#    
#    return [
#            -(K[C1,O1] + K[C1,C2])*Nc1 + K[O1,C1]*No1 + K[C2,C1]*Nc2, # = Nc1'
#            -(K[O1,C1] + K[O1,O2])*No1 + K[C1,O1]*Nc1 + K[O2,O1]*No2, # = No1'
#            -(K[O2,O1] + K[O2,C2])*No2 + K[O1,O2]*No1 + K[O2,O2]*No2, # =No2'
#            -(K[C2,C1] + K[C2,O2])*Nc2 + K[O1,C2]*No1 + K[C1,C2]*Nc1, # =Nc2'
#            ]
          
def three_states_with_light(n,t):
    # Is the transition light activated?
    C = 0   # Closed state
    O = 1   # Opens state
    D = 2   # Desensitized state
    
    # just renameing for better convenience
    Nc = n[0]
    No = n[1]
    Nd = n[2]    
    
    # transition coefficients when light is OFF
    K_off = zeros([3,3])
    # transition coefficients when light is ON
    K_on =  zeros([3,3])

    tau = 20 # tau off [ms]
    v = 1.0/tau
    
    K_off[O,C] = v
    K_on[O,C] = K_off[O,C]    
    
    K_off[C,O] = 0
    K_on[C,O] = v*power*alpha
    
    K_off[O,D] = v/4
    K_on[O,D] = K_off[O,D]
    
    K_on[D,O] = 0
    K_off[D,O] = 0

    K_off[C,D] = 0        
    K_on[C,D] = 0

    K_off[D,C] = v/1000
    K_on[D,C] = K_off[D,C]
    
    if t < time_before_pulse:
        K = zeros([3,3])
    elif t < time_before_pulse+pulse_duration:
        K = K_on
    else:
        K = K_off

    
    return [
            -K[C,O]*Nc -K[C,D]*Nc +K[D,C]*Nd +K[O,C]*No, # = Nc'
            -K[O,D]*No -K[O,C]*No +K[C,O]*Nc +K[D,O]*Nd, # = No'
            -K[D,C]*Nd -K[D,O]*Nd +K[O,D]*No +K[C,D]*Nc # =Nd'
            ]
            
            
#def three_states_with_light_matrixform(n,t):
#    # Is the transition light activated?
#    C = 0   # Closed state
#    O = 1   # Opens state
#    D = 2   # Desensitized state
#    
#    # just renameing for better convenience
#    Nc = n[0]
#    No = n[1]
#    Nd = n[2]
#    N = [Nc,No,Nd]
#    
#    pulse_duration = 0.5
#
#    
#    
#    # transition coefficients when light is OFF
#    K_off = zeros([3,3])
#    # transition coefficients when light is ON
#    K_on =  zeros([3,3])
#
#    v = 1/0.1
#    
#    K_on[O,C] = 0
#    K_off[O,C] = 0
#    
#    K_on[C,O] = v*5
#    K_off[C,O] = 0
#
#    K_on[O,D] = v
#    K_off[O,D] = v
#    
#    K_off[D,O] = 0
#    K_on[D,O] = 0
#    
#    K_on[C,D] = 0
#    K_off[C,D] = 0    
#
#    K_on[D,C] = v
#    K_off[D,C] = v
#    
#    if t < pulse_duration:
#        K = K_on
#    else:
#        K = K_off
#
#    M = transpose(K)
#    for it in size(M,1):
#        M[it,it] = 
#    
#    return []            
            
            



func = three_states_with_light


figure(1)
# Experimental data
folder = '/home/marco/Desktop/ospin_model/experimental/'
filename = 'cell_100_ChR2.txt'
n_data = 0

(names,data) = parseDatafile(folder+filename,'\t')
datalength = len(data[names[n_data]])

# ascisse
#t = linspace(0,1,len(data[names[0]]))
dt = 0.1 #acquisition time [ms]
t = [it*dt for it in range(0,datalength)]

#power
powervalues = array([
0.0836012861736334,
0.0498392282958199,
0.0249196141479099,
0.0160771704180064,
0.0128617363344051,
0.00803858520900321,
0.00643086816720257,
0.00321543408360128,
0.00160771704180064,
])
#powervalues = powervalues[::-1]/min(powervalues)
# Kon = Koff*alpha*power
alpha = 26
power = powervalues[n_data]
print 1/(1.0/20*power*alpha)

plot(t,data[names[n_data]],'.')
# MODEL
# popolazione totale
N = 300
time_before_pulse = 116
pulse_duration = 206-time_before_pulse

yinit = [N,0,0]
y = odeint(func,yinit,t)

#plot(t,y[:,0],t,y[:,1])
#plot(t,y[:,0],'r',t,y[:,1])
plot(t,-y[:,1],'r')
#axis([-0.1,1,-1,1])
show()