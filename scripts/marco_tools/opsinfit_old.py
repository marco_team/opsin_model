__author__ = 'marco'

from scipy.optimize import curve_fit
from numpy import exp
import numpy as np
from Experiment import Experiment
import matplotlib.pyplot as ppt
from OpsinExperiment import OpsinExperiment

class FitOpsinExperiment(list):

    def __init__(self,e):
        if isinstance(e,OpsinExperiment):
            self.tau_off = None
            self.tau_on = None
            self.its = None
            self.ite = None
            self.e = e
            # if the pulse end was not defined in the OpsineExperiment, find it
            if self.e.pulse_end == None:
                self.e.pulse_end = self.e.pulse_end_find()

            super(FitOpsinExperiment, self).__init__()
        else:
            raise Exception('experimentCollection only contains object of class experiment')

    def __getitem__(self, key):
        return super(FitOpsinExperiment, self).__getitem__(key)

    def __setitem__(self, key, value):
       super(FitOpsinExperiment, self).__setitem__(key,value)

    def append(self, p_object):
    # append an experiment in the collection
        super(FitOpsinExperiment, self).append(p_object)


    def fit_tau_off(self,t_fit_start=None,t_fit_end=None):
        """
        find tau-off from a given opsineExperiment
        by fitting with the function -exp(-ax) : NOTE THTAT THE CURENT MUST BE POSITIVE
        pulse off time must be specified
        """
        e=self.e
        if t_fit_start == None:
            t_fit_start = e.pulse_end
        if t_fit_end == None:
            t_fit_end == e.current_time_space[-1]

        # find index of t_fit_strat
        for its_temp,t in enumerate(e.current_time_space):
            if t > t_fit_start:
                self.its = its_temp
                break

        # find index of t_fit_end
        for ite_temp,t in enumerate(reversed(e.current_time_space)):
            if t < t_fit_end:
                self.ite = len(e.current_time_space)-ite_temp
                break

        data = e.y[self.its:self.ite]
        timespace = e.current_time_space[self.its:self.ite]

        popt, pcov = curve_fit(
            lambda x,a0,a1 : a0 * np.exp(-a1*(x - t_fit_start)),  # fit function
            timespace,            # y values
            data,                            # data to fit
            p0 = [1,1],             # initial guess for the parameters

            #sigma = 0.01*ones(shape(t))      # weights (see documentation)
        )

        # empty the content of the list which contatins the fit courbe
        del self[:]

        for i in (popt[0] * exp(-popt[1]*(e.current_time_space - t_fit_start)))[self.its:self.ite]:
            self.append(i)

        self.timespace = e.current_time_space[self.its:self.ite]
        self.tau_off = 1.0/popt[1]
        self.popt = popt



    def fit_tau_on(self,t_fit_start=None,t_fit_end=None,stop_at_max_ratio=0.5):
        """
        find tau-off from a given opsineExperiment
        by fitting with the function -exp(-ax) : NOTE THTAT THE CURENT MUST BE POSITIVE
        pulse off time must be specified
        """

        e=self.e
        m = np.max(e.y)
        if t_fit_start == None:
            t_fit_start = e.pulse_start_find()
        if t_fit_end == None:
            # find the time at which the data is above the half maximum
            for temp_ite,x in enumerate(e.y):
                if x > m*stop_at_max_ratio :
                    t_fit_end = e.current_time_space[temp_ite]
                    break


        # index of t_fit_strat
        for its_temp,t in enumerate(e.current_time_space):
            if t>t_fit_start:
                self.its = its_temp-1
                break
        # find index of t_fit_end
        for ite_temp,t in enumerate(e.current_time_space):
            if t>t_fit_end:
                self.ite = ite_temp
                break

        data = e.y[self.its:self.ite]
        timespace = e.current_time_space[self.its:self.ite]

        try:
            popt, pcov = curve_fit(
            lambda x,a0 : -1 * np.exp(-a0*(x - t_fit_start)) + 1,  # fit function
                timespace,            # y values
                data,                            # data to fit
                #p0 = [1,1,m],             # initial guess for the parameters

                #sigma = 0.01*ones(shape(t))      # weights (see documentation)
            )
        except RuntimeError:
            popt = [0,0.1,0]
            pcov = None


        # empty the content of the list which contatins the fit courbe
        del self[:]

        for i,x in enumerate(self.e.current_time_space):
            if x>self.e.pulse_end:
                index_pulse_end = i
                break

        for i in (-1 * exp( -popt[0] * ( e.current_time_space - t_fit_start) ) + 1 )[self.its:index_pulse_end]:
            self.append(i)

        self.timespace = e.current_time_space[self.its:index_pulse_end]
        self.tau_on = 1.0/popt[0]
        #self.plateau_current = popt[2]
        self.popt = popt







    def fit_tau_on_for_normalization(self,t_fit_start=None,t_fit_end=None,stop_at_max_ratio=0.5):
        """
        find tau-off from a given opsineExperiment
        by fitting with the function -exp(-ax) : NOTE THTAT THE CURENT MUST BE POSITIVE
        pulse off time must be specified
        """

        e=self.e
        m = np.max(e.y)
        if t_fit_start == None:
            t_fit_start = e.pulse_start_find()
        if t_fit_end == None:
            # find the time at which the data is above the half maximum
            for temp_ite,x in enumerate(e.y):
                if x > m*stop_at_max_ratio :
                    t_fit_end = e.current_time_space[temp_ite]
                    break


        # index of t_fit_strat
        for its_temp,t in enumerate(e.current_time_space):
            if t>t_fit_start:
                self.its = its_temp-1
                break
        # find index of t_fit_end
        for ite_temp,t in enumerate(e.current_time_space):
            if t>t_fit_end:
                self.ite = ite_temp
                break

        data = e.y[self.its:self.ite]
        timespace = e.current_time_space[self.its:self.ite]

        try:
            popt, pcov = curve_fit(
            lambda x,a0,a1,a2 : -a0 * np.exp(-a1*(x - t_fit_start)) + a2,  # fit function
                timespace,            # y values
                data,                            # data to fit
                #p0 = [1,1,m],             # initial guess for the parameters

                #sigma = 0.01*ones(shape(t))      # weights (see documentation)
            )
        except RuntimeError:
            popt = [0,0.1,0]
            pcov = None


        # empty the content of the list which contatins the fit courbe
        del self[:]

        #find index of pulse end
        for i,x in enumerate(self.e.current_time_space):
            if x>self.e.pulse_end:
                index_pulse_end = i
                break

        #index_pulse_end = self.e.current_time_space.tolist().index(self.e.pulse_end)
        for i in (-popt[0] * exp( -popt[1] * ( e.current_time_space - t_fit_start) ) + popt[2] )[self.its:index_pulse_end]:
            self.append(i)

        self.timespace = e.current_time_space[self.its:index_pulse_end]
        self.tau_on = 1.0/popt[1]
        #self.plateau_current = popt[2]
        self.popt = popt



    @property
    def bounds(self):
        """
        return the fit boundaries as a touple.
        The first value of the touple is a touple of 2 abscisses
        the second value of the touple is a touple of 2 ordinates

        Example of use:
        plot(f_tau_on.fit_bounds[0],f_tau_on.fit_bounds[1],'ro')
        :return:
        """
        l = ()
        if (self.its!=None) & (self.ite!=None):
            l=  (self.e.current_time_space[self.its], self.e.current_time_space[self.ite]),\
                (self.e.y[self.its], self.e.y[self.ite] )
        return l



if __name__ == "__main__":
    from ExperimentFile import ExperimentFile
    from ExperimentFolder import ExperimentFolder

    folder = "../../experimental/CoChr_powerCurve/"

    folder = ExperimentFolder(folder)
    filename = folder.file_names_as_string_list[2]
    efile = ExperimentFile(folder.folder, filename)

    e = OpsinExperiment(efile,0,1000)

    e.set_data_density(step=200,mean_between_steps=True)
    e.y.substract_mean(200,len(e.y))
    e.y.invert()
    e.y /= max(e.y)

    print e.current_time_space[-1], e.pulse_start, e.pulse_end
    f_tau_off = FitOpsinExperiment(e)
    f_tau_off.fit_tau_off(t_fit_end=800)

    f_tau_on = FitOpsinExperiment(e)
    f_tau_on.fit_tau_on(t_fit_start=104)

    #params = parseDatafile(folder + '/' + 'parameters.dat' ,'\t')
    ppt.plot(e.current_time_space,e.y)
    ppt.plot(f_tau_off.timespace,f_tau_off,'r',label=f_tau_off.tau_off)

    ppt.plot(f_tau_off.bounds[0],f_tau_off.bounds[1],'ro')
    ppt.plot(f_tau_on.bounds[0],f_tau_on.bounds[1],'ro')

    ppt.plot(f_tau_on.timespace,f_tau_on,'k',label=f_tau_on.tau_on)
    #
    ppt.legend()
    ppt.show()

