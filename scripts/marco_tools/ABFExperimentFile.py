__author__ = 'marco'

import numpy as np
from neo.io import AxonIO

class ABFExperimentFile:
    """
    model a multi-column experiment file
    """

    def __init__(self,file_name):
        # set folder and file names
        self.file_name = file_name

    def import_data(self, block=0, segment=0, signal=0):
        """
        parse the CSV data file.
        """
        r = AxonIO(filename=self.file_name)
        bl = r.read()[block]
        seg = bl.segments[segment]
        sig = seg.analogsignals[signal]

        return sig.times, sig

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import OpsinExperiment

    filename = '../../experimental/abf_test/20160127_0016.abf'


    f = ABFExperimentFile(filename)
    xdata, ydata = f.import_data(block=0, segment=0, signal=0)
    e = OpsinExperiment.OpsinExperiment(ydata)


    e.set_data_density(50, mean_between_steps=True)

    plt.plot(e.y)

    plt.legend()
    plt.show()