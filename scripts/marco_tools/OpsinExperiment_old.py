__author__ = 'marco'

from Experiment import Experiment
import numpy as np

class OpsinExperiment(Experiment):
    def __init__(self,experiment_file, t_start, t_end):
        self._t_start = t_start
        self._t_end = t_end
        self.light_power = None
        self.pulse_start = None
        self.pulse_end = None
        self._dt = None
        self._datalength = None
        super(OpsinExperiment, self).__init__(experiment_file)

    @property
    def t_start(self):
        return self._t_start
    @property
    def t_end(self):
        return self._t_end

    def set_data_density(self,n=0, step=1, mean_between_steps=False):
        super(OpsinExperiment, self).set_data_density(n,step,mean_between_steps)
        self._dt = self.calculate_dt(self.t_start,self.t_end,len(self.y))
        self._datalength = len(self.y)

    # TODO implement this
    # def select_by_time(self,start=None,end=None):
    #     if (start == None) : start = self._t_start
    #     if (end == None)   : end = self._t_end
    #
    #
    #     pass




    def pulse_start_find(self):
        """
        Try to find the pulse starts time, as the last point where the y is zero before the peak.
        :return:
        """
        temp_max = np.max(self.y)
        beyond_max = False
        rev = self.y[::-1]
        for i,x in enumerate(rev):
            if not beyond_max:
                if x == temp_max : beyond_max = True
                continue
            elif x < 0 :
                #i = i + 1
                break
        i = len(self.current_time_space) - i # the list was reversed
        return self.current_time_space[i] , i
    
    def pulse_end_find(self):
        """
        Try to find the time the pulse ends.
        :return:
        """
        old_x = self.y[-1]
        m = np.max(self.y)/2
        for i,x in enumerate(reversed(self.y)):
            if x<m:
                continue
            if x < old_x:
                i = len(self.current_time_space) - i
                break
            old_x = x

        return self.current_time_space[i], i

    @property
    def current_time_space(self):
        #return np.linspace(self.t_start,self.t_end, len(self.y) )
        return np.linspace(self.t_start,len(self.y)*self._dt,len(self.y))

    @property
    def dt(self):
        return self._dt

    def calculate_dt(self,start,end,n):
        return (end-start)*1.0/n

    @property
    def CSV_column_headers(self,delimiter='\t'):
        return delimiter.join(['file_name','header','comment','t_start','t_end','pulse_start','pulse_end','current_vs_time'])

    def data_to_string(self):
        return ','.join([str(num) for num in self.y[::] ] )

    def to_CSV_line(self,delimiter='\t'):
        s = delimiter.join([self.filename,
                       self.header,
                       self.comment,
                       str(self.t_start),
                       str(self.t_end),
                       str(self.pulse_start),
                       str(self.pulse_end),
                       self.data_to_string()])
        return s