__author__ = 'marco'

from parsedatafile import parse_datafile
import numpy as np
import os

class ExperimentFile:
    """
    model a multi-column experiment file
    """

    def __init__(self,folder_name,file_name, delimiter='\t', comment_char='#', quotechar='\\', cast='float', first_line_is_column_headers=True):
        # set folder and file names
        if folder_name[-1] != os.path.sep : folder_name = '{}{}'.format(folder_name,os.path.sep)
        self.file_name = file_name
        self.folder_name = folder_name
        self.full_path = folder_name + file_name

        # import data
        self.column_headers, self.column_data = self.import_data(delimiter=delimiter,
                                                                  comment_char=comment_char,
                                                                  quotechar=quotechar,
                                                                  cast=cast,
                                                                  first_line_is_column_headers=first_line_is_column_headers)

    def import_data(self, **kwargs):
        """
        parse the CSV data file.
        """
        headers, data = parse_datafile(self.full_path,
                                      delimiter=kwargs['delimiter'],
                                      comment_char=kwargs['comment_char'],
                                      quotechar=kwargs['quotechar'],
                                      cast=kwargs['cast'],
                                      first_line_is_column_headers=kwargs['first_line_is_column_headers'] )
        return headers, np.array(data)

    @property
    def column_number(self):
        return len(self.column_data)

    #
    # def invert_data(self):
    #     for i,e in enumerate(self.column_data):
    #         self.column_data[i] = -e
    #
    # def substract_constant(self,c):
    #     for i,data in enumerate(self.column_data):
    #         self.column_data[i] -=c
    #
    # def substract_mean(self,start_x=None,end_x=None):
    #     if start_x == None: start_x = 0
    #     if end_x == None:
    #         # calcualte the mean on given interval
    #         for i,data in enumerate(self.column_data):
    #             self.column_data -= np.mean(data[start_x:])
    #     else:
    #         # calcualte the mean on given interval
    #         for i,data in enumerate(self.column_data):
    #             self.column_data -= np.mean(data[start_x:])


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from ExperimentFolder import ExperimentFolder
    # folder = '../../experimental'
    # filename = 'cell_100_ChR2.txt'

    folder = "../experimental/CoChr_powerCurve"
    expFolder = ExperimentFolder(folder)
    filenames = expFolder.file_names_as_string_list

    for filename in filenames:
        # print filename
        try:
            f = ExperimentFile(folder,filename,first_line_is_column_headers=False)
        except:
            print 'exception:', filename
        #print 'column number : ', f.column_number
        # print f.column_data[0][0]
        plt.plot(f.column_data[0],label=f.column_headers[0])

    plt.legend()
    plt.show()