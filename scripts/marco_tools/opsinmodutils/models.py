from numpy import empty_like, dot, shape
from utils import *
from my_linalg_utils import *

__author__ = 'marco'


# def ode_model(y, t, model="marco"):
#     """
#     ODE model fot a 2 level system
#     """
# if model == "marco" :
#     # A two state model from scratch
#     dy = array([
#         y[0]*(-B01*n_pulse(t)) + y[1]*A10,
#         y[0]*(+B01*n_pulse(t)) - y[1]*A10
#     ])
#
# elif model == "benoit":
#     # Benoit's original descrtipion of a three levels system
#     """
#     dy=array([  A20*(1-y[0]-y[1]) + A10*y[1] - B01*n_pulse(t)*y[0],
#             B01*n_pulse(t)*y[0] - A10*y[1] - A12*y[1] ,
#             -A20*(1-y[1]-y[0]) +A12*y[1] ])
#     """
#     # same description, different factorization (why 3 equations for a 2 level system???)
#
#     dy = array([
#         y[0]*(-A20 - B01*n_pulse(t)) + y[1]*( -A20 + A10 ) + A20,
#         y[0]*B01*n_pulse(t)          + y[1]*( -A10 - A12 ) + 0,
#         y[0]*A20                     + y[1]*(  A20 + A12 ) - A20
#     ])
# return dy

def ode_model_generic(y, t, k_off, k_on, pulse=None):
    """
    ODE model for the population in a system of N levels
    :type pulse: object of class Pulse in opsinmodutils/tils.py
    :param y: list of functions in the linear system (1xN)
    :param t: time vector
    :param k: coefficient matrix (NxN) k_on and k_off
    :return: the derivatives of y
    """

    # test that the given matrix and the vector have the right dimensions
    test = square_matrix_and_vector_dimensions_match
    if test(k_off,y) and test(k_on,y):
        raise ValueError('Error in k_on, k_off or y dimesions\n' \
                        'if len(y) = D, k dimentions must be DxD' )
        exit(1)

    dy = empty_like(y)

    if pulse != None:
        t_start = pulse.t_start
        t_rep = pulse.t_rep
        t_dur = pulse.t_dur
    else:
        t_start = 10
        t_dur = 50
        t_rep = 100

    # chose the k matrix depending on the light pulse being ON or OFF
    if n_pulse(t,t_start,t_dur,t_rep) == 0:
        k = k_off
    else:
        k = k_on

    # ODE system
    for i, f in enumerate(y):
        dy[i] = -f * sum(k[i, :]) + dot(y, k[:, i])  # short notation for the linear system
        # it is easy to write

    return dy




#
# def ode_model_generic_on_off(y, t, k):
#     # test that the given matrix and the vector have the right dimensions
#     # test = square_matrix_and_vector_dimensions_match
#     # if test(k,y):
#     #     raise ValueError('Error in k_on, k_off or y dimesions\n' \
#     #                     'if len(y) = D, k dimentions must be DxD' )
#     #     exit(1)
#
#     dy = empty_like(y)
#
#     # ODE system
#     for i, f in enumerate(y):
#         dy[i] = -f * sum(k[i, :]) + dot(y, k[:, i])  # short notation for the linear system
#
#     return dy



def ode_model_test():
    from scipy.integrate import odeint

    k_on = zeros((2, 2))
    k_on[0, 1] = 0.1
    k_on[1, 0] = 1

    k_off = zeros((2, 2))
    k_off[0, 1] = 0
    k_off[1, 0] = 1

    t = linspace(0, 100, 1000)

    args = (k_off, k_on)
    v = odeint(ode_model_generic, [1., 0.], t, args)

    plot(t, v[:, 1])
    show()


def ode_model_test_3():
    from scipy.integrate import odeint
    from numpy import copy

    k_off = zeros((3,3))
    k_off[1,2] = 0.15
    k_off[1,0] = 0.25
    k_off[2,0] = 0.5
    k_on = copy(k_off)
    k_on[0,1] = 10

    t = linspace(0, 500, 1000)

    args = (k_off, k_on)
    v = odeint(ode_model_generic, [200.0, 0.,0], t, args)

    plot(t, v[:, 1])
    show()

if __name__ == "__main__":
    ode_model_test_3()
