from numpy import shape, zeros, where, array, int8, linspace
from matplotlib.pyplot import plot, xlabel, ylabel, axis, show


def pulse(t,tstart=10,tdur=50):
    """
    Returns the pulse status (0 = off, 1 = on), according to the given parameters.
    This function implements a single pulse (for a pulse train, use n_pulse())
    """

    pulse_stauts = (t>tstart) & (t<(tstart+tdur))

    if shape(t) == () :
        if pulse_stauts :
            pulse = 1.0
        else:
            pulse = 0.0
    else:
        pulse=zeros(shape(t))
        pulse[where(pulse_stauts) ] = 1.0
    return pulse


def n_pulse(t, tstart=10, tdur=50, trep=100.0,increasing=False):
    """
    Returns the pulse status (0 = off, 1 = on), according to the given parameters.
    This function implements repeated pulses.
    If t is a scalar it returns a scalar.
    If t is a sequence, returns a sequence.

    ARGUMENTS
    tstart : starting time of the pulse in time units ( data collection starts at t = 0 )
    tdur   : pulse duration in time units
    trep   : repetition time (one pulse every trep time units)
    increasing : if true, the pulse amplitude will increase with time

    Return type : same type of t.
    """

    t = array(t)

    # if this condition is true at time t, the light is on
    # dependig on the shape of t, it can be a scalar or a list
    pulse_status = ( t % trep > tstart ) & ( t % trep < ( tstart + tdur ))

    if shape(t) == () :   # if t is a scalar
        if ( pulse_status ):
            pulse = 1.0 + int8( t / trep )*increasing # why the pulse increases ?
        else:
            pulse = 0.0
    else:                 # if t is not a scalar
        pulse = zeros( shape(t) )
        pulse[ where( pulse_status ) ] \
            = 1.0 + int8( t[ where( pulse_status ) ] / trep )*increasing

    return pulse


class Pulse:
    """
    represents a light pulse
    """
    def __init__(self,t_start,t_dur,t_rep):
        self.t_start = t_start
        self.t_rep = t_rep
        self.t_dur = t_dur


# TESTS
def n_pulse_test() :
    print n_pulse(1)
    # print shape(1)==()
    print n_pulse(20)

    print n_pulse([1,5,10,20,100])

    t = linspace(0,1000,1000)
    plot(t,n_pulse(t))
    xlabel('time units')
    ylabel('n_pulse() return')
    axis([min(t),max(t),0,2])
    show()

def pulse_test() :
    print pulse(1)
    # print shape(1)==()
    print pulse(20)

    print pulse([1,5,10,20,100])

    t = linspace(0,1000,1000)
    plot(t,pulse(t))
    xlabel('time units')
    ylabel('pulse() return')
    axis([min(t),max(t),0,2])
    show()


if __name__ ==  "__main__":
    n_pulse_test()
    pulse_test()

