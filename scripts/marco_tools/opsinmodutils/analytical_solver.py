__author__ = 'marco'

from numpy import exp, empty_like, outer, copy, zeros, matrix, linspace
from numpy.linalg import det, eig, solve
from utils import n_pulse

from my_linalg_utils import *


class Solver:
    """
    Analytical solver for linear ODE system
    """

    def __init__(self, k_matrix, initial_condition):
        """
        :param initial_condition: initial state of the system
        :param k: coefficient matrix
        :return:
        """
        test = square_matrix_and_vector_dimensions_match
        if test(k_matrix, initial_condition):
            raise ValueError('k_matrix and initial_conditions dimesions mismatch')
            exit(1)
        else:
            self.k_matrix = k_matrix
            self.ic = initial_condition

            self.size = len(initial_condition)

            self.system_coeff_matrix = self.build_system_coeff_matrix() # the linear system coefficient matrix

            self.k_eigenvalues, self.k_eigenvectors = eig(self.system_coeff_matrix)
            self.k_eigenvectors = self.k_eigenvectors.transpose()
            self.k_det = det(self.system_coeff_matrix)

            self.solution_coefficients = self.calculate_solution_coefficients()

    def build_system_coeff_matrix(self):
        coeffm = empty_like(self.k_matrix)
        for row in range(coeffm.shape[0]):
            for col in range(coeffm.shape[1]):
                if row == col:
                    coeffm[row,col] = -sum(self.k_matrix[row,:])
                else:
                    temp = self.k_matrix.transpose()
                    coeffm[row,col] = temp[row,col]
        return coeffm

    def calculate_solution_coefficients(self):
        """
        Calculate the solution_coefficients for the problem solution with the initial condition
        :return: None
        """
        try:
            temp = solve(self.k_eigenvectors.transpose(), self.ic)
        except Exception:
            print "Error in solution coefficient calculation."
            exit(1)
        return temp

    def solution(self,t):
        """
        Returns the solution of this problem.
        The type of return is the same type of t
        :param t: time (scalar or numpy array)
        :return:
        """

        # distinguish if the time is a scalar or a vector
        if t.shape == ():
            s = 0
        else:
            s = zeros((self.size,len(t)))
        for i in range(self.size):
            s += self.solution_coefficients[i]* \
                 outer(self.k_eigenvectors[i], exp( self.k_eigenvalues[i]*t ) ) # outer product is needed here

        return s


class PulsedSolver:
    def __init__(self,k_on,k_off,initial_conditions,t,pulse=None):
        self.k_on = k_on
        self.k_off = k_off
        self.ic = initial_conditions
        self.t = t

        if pulse != None:
            self.t_start = pulse.t_start
            self.t_rep = pulse.t_rep
            self.t_dur = pulse.t_dur
        else:
            self.t_start = 10
            self.t_dur = 50
            self.t_rep = 100





    def solution(self):
        """
        :return: the solution as a NxM array where M is the problem dimention, N is the length of the time array
        """
        new_start_t = 0
        start_status = n_pulse(self.t,self.t_start,self.t_dur,self.t_rep)[new_start_t]
        new_ic = self.ic
        solution = zeros( (len(self.ic),len(self.t)) )
        timefromzero = linspace(0,self.t[-1]-self.t[0],len(self.t))

        for t,status in enumerate(n_pulse(self.t,self.t_start,self.t_dur,self.t_rep)):
            if status != start_status:  # if pulse state changes write this part of the solution
                if start_status:          # select the right k matrix (corresponding to the start pulse status)
                    k = self.k_on
                else:
                    k = self.k_off

                s = Solver(k, new_ic)   # Create a new solver with current k matrix

                # calculate the solution on the previous time interval
                # note that t is an index, self.t is a linear-space
                temp = timefromzero[0:(t - new_start_t)]
                # print new_start_t,  t-1,(t-1 - new_start_t), len(temp)
                solution[:, new_start_t : t] = s.solution( temp )

                # set the starting parameters for the new time interval
                new_ic = s.solution( timefromzero[t-1 - new_start_t] )
                new_start_t = t
                start_status = status


        # at the end fill the remaining part of the solution with the current status
        if start_status:
            k = self.k_on
        else:
            k = self.k_off

        s = Solver(k, new_ic)
        temp = timefromzero[0:(t - new_start_t)]
        solution[:, new_start_t : t] = s.solution( temp )
        return solution

def test_static_power():

    # Marco's Model
    k = zeros((3,3))
    k[1,2] = 0.15
    k[1,0] = 0.25
    k[2,1] = 0.5
    k[0,1] = 10
    ic = array([1,0,0])
    s_marco = Solver(k, ic)

    # 3 levels system
    k = zeros((3,3))
    k[1,2] = 0.15
    k[1,0] = 0.25
    k[2,0] = 0.5
    k[0,1] = 10
    ic = array([1,0,0])

    # two levels
    # k = zeros((2,2))
    # k[0,1] = 0.4
    # k[1,0] = 0.4
    # ic = array([1,0])

    s = Solver(k, ic)
    print "coeff matrix"
    print s.system_coeff_matrix

    print "eigenvectors:"
    print s.k_eigenvectors
    print s.k_eigenvectors[0]
    #print s.k_eigenvectors.transpose()
    print "eigenvalues:"
    print s.k_eigenvalues
    print "solution_coefficients:"
    print s.solution_coefficients
    print "size:"
    print s.size
    print "solution"

    time = linspace(5,500,1000)

    y_marco = [s_marco.solution(t)[1] for t in time]  # here time is a scalar
    y = s.solution(time)[1] # here time is an array
    # plot(time,y,time,y_marco,'-')
    k_off = copy(k)
    k_off[0,1] = 0
    ps = PulsedSolver(k,k_off,[1,0,0],time)
    psy = ps.solution()

    # print time.shape , psy.shape, s.solution(time).shape

    # print psy.shape, y.shape
    print len(psy)
    plot(time,psy[1], '-')

    show()

def test_dynamic_power():
    time = linspace(0,500,1000)
    step = time[0] - time[1]


    # Marco's Model
    # Marco's Model
    k = zeros((3,3))
    k[1,2] = 0.3
    k[1,0] = 8
    k[2,1] = 0.5
    kon = 5
    ic = array([1,0,0])

    y = empty_like(time)

    for i,t in enumerate(time):
        if t < 250 :
             k[0,1] = kon
        else :
             k[0,1] = kon*(1+0.5*sin((t-250)*5*2*pi/250))


        s_marco =  Solver(k,ic)
        y[i] = s_marco.solution(t)[1]
        #ic = (s_marco.solution(step)[0],s_marco.solution(step)[1],s_marco.solution(step)[2])
        s_marco.k_matrix = k


    # s_marco =  Solver(k,ic)
    # y = [s_marco.solution(t)[1] for t in time]
    plot(time,y,'-')

    show()



if __name__ == "__main__":
    from numpy import array, zeros, linspace, empty_like, pi, sin
    from matplotlib.pyplot import plot, show

    test_static_power()
    # test_dynamic_power()