__author__ = 'marco'

from my_linalg_utils import *


def square_matrix_and_vector_dimensions_match(m,v):
    """
    compare a square matrix with a vector to see if the dimensions are equal,
    that is, if the matrix can operate on that vector
    :param m: square matrix
    :param v: vector (row or column)
    :return:
    """

    # m must a be square matrix
    # len(v) must be the same as the dimension of m

    dimensions_match = ( m.shape != (len(v),len(v)) )

    return dimensions_match