# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 14:47:08 2015
Simple script for importing and plotting data from a file
@author: marco

this script opens a CSV datafile and digest itm returning a Python type

"""

import csv
import logging




def cast_list_of_string(l,to_type):
    """
    cast a list of string in a list of type_type
    :param l:
    :param to_type: a string indicating the return type for each list element (it can be 'int' or 'float')
    :return:
    """

    #remove extra white spaces
    for i,el in enumerate(l):
        for s in el:
            s.strip()

    if to_type == 'float':
        for i,el in enumerate(l):
            l[i] = map(float,el)
    if to_type == 'int':
        for i,el in enumerate(l):
            l[i] = map(int,el)

def parse_datafile(datafile, delimiter, comment_char='#', quotechar='\\',cast=None, first_line_is_column_headers=True):
    """
    Open a datafile and return a list of data
    :param datafile:
    :param delimiter:
    :param quotechar:
    :return: list
    """
    with open(datafile,'rb') as csvfile:
        # conunt column number
        reader = csv.reader(csvfile, delimiter=delimiter, quotechar=quotechar)
        firstline = reader.next()
        column_number = len(firstline)

        # collect the column header if they exist
        column_headers = ['column_{}'.format(str(num)) for num in range(column_number)]

        if first_line_is_column_headers:
            # put cursor to first line
            csvfile.seek(0)
            column_headers = reader.next()

        # a list of lists. Each sublist represents a column in the file
        digested = [[] for i in range(0,column_number)]

        for rn,row in enumerate(reader):
            try:
                if row[0][0] != comment_char:
                    for it,elm in enumerate(row):
                        digested[it].append(elm)
            except:
                logging.warning("PARSE-DATAFILE ERROR file: {filename}\n"
                      "newline encountered while parsing datafile\n"
                      "inserted previous valid data value instead of None\n".format(filename=datafile))
                digested[it].append(elm)
        if cast != None:
            cast_list_of_string(digested,cast)

    return column_headers, digested



def parseDatafile_sniff(datafile,cast=None):
    with open(datafile, 'rb') as df:
        dialect = csv.Sniffer().sniff(df.read(16))
        # put cursor on files first line
        df.seek(0)
        # Analyze the sample text (presumed to be in CSV format) and return True if the first row appears to be a series of column headers.
        has_header = csv.Sniffer().has_header(df.read(1024))

        # count the number of columns
        df.seek(0)
        reader = csv.reader(df,dialect)
        firstline = reader.next()
        column_number = len(firstline)

        # collect the column header if they exist
        column_headers = ['column_{}'.format(str(num)) for num in range(column_number)]
        df.seek(0)
        if has_header:
            column_headers = reader.next()

        digested = [[] for i in range(0,column_number)]

        for rn,row in enumerate(reader):
            for it,elm in enumerate(row):
                digested[it].append(elm)

        if cast != None:
            cast_list_of_string(digested,cast)


    return column_headers,digested




if __name__ == "__main__":
    import matplotlib.pyplot as plt
    # folder = '../../experimental/'
    # filename = 'cell_100_ChR2.txt'

    folder = "../../experimental/CoChr_powerCurve/"
    filename = "50.txt"

    # test parseDatafile()
    headers, data = parse_datafile(folder + filename,'\t',first_line_is_column_headers=True)
    # test parseDatafile_sniff()
    # headers_sniff, data_sniff = parseDatafile_sniff(folder + filename)

    x_val = range(0,len(data[0]))
    plt.subplot(2,1,1)
    plt.plot(x_val,data[0],'.',label=headers[0])
    plt.legend()
    plt.title('parseDatafile() test')
    #
    # plt.subplot(2,1,2)
    # plt.plot(x_val,data_sniff[0],'.',label=headers_sniff[0])
    # plt.legend()
    # plt.title('parseDatafile()_sniff test')

    plt.show()