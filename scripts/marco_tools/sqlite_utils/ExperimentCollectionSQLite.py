__author__ = 'marco'



class ExperimentCollectionSQLite(list):

    def __init__(self):
        super(ExperimentCollectionSQLite, self).__init__()

    def append(self, p_object):
        # check type
        super(ExperimentCollectionSQLite, self).append()

    def check_element_type(self,e):
        """
        check if the type of element e is opsinExperiment
        :return:
        """
        pass

    def OpsineExperiment_DAO(self):
        # converte un entry di un DB sql in un oggetto opsineExperiment
        pass

    def load_from_SQLite(self):
        # carica la collezione di esperimenti da un file SQLite
        pass

    def write_to_SQLite(self):
        # salva questa collezione in un file SQLite
        pass

if __name__ == "__main__":
    exps = ExperimentCollectionSQLite()
    exps.append(1)
    print len(exps)
    pass

