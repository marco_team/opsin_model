__author__ = 'marco'

import csv

def list_to_CSV(data_list,filename, column_titles=None, comment_char='#'):
    with open(filename,'wb') as file_obj:
        csv_writer = csv.writer(file_obj,delimiter='\t')

        if column_titles != None:
            column_titles[0] = '{}{}'.format(comment_char,column_titles[0])
            csv_writer.writerow(column_titles)
        # spanning the data length
        for line_n in range(len(data_list[0])):
            row = []
            # spanning the data sets
            for e in data_list:
                row.append("{}".format(e[line_n]))
            csv_writer.writerow(row)


if __name__ == "__main__":
    pass