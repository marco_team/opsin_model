__author__ = 'marco'

from parsedatafile import parse_datafile
from listToCSV import list_to_CSV
import numpy as np
import os
from ExperimentFolder import ExperimentFolder
from ExperimentFile import ExperimentFile


class Experiment(object):
    """
    an experiment has a data file (CSV) associated with
    can parse the data file
    """

    def __init__(self, experiment_file):
        """
        experiment_file is an object of type experimentFile
        """
        # self.data=[]
        self.experiment_file = experiment_file
        self.filename = experiment_file.file_name
        self.current = self.Current(np.arange(0))
        #self.set_current(0)
        self.header = '' # the header of this experiment
        self.comment = ''

    class Current(np.ndarray):
        def __new__(cls, input_array, info=None):
            # Input array is an already formed ndarray instance
            # We first cast to be our class type
            obj = np.asarray(input_array).view(cls)
            # add the new attribute to the created instance
            obj.info = info
            # Finally, we must return the newly created object:
            return obj
        
        def __array_finalize__(self, obj):
            # see InfoArray.__array_finalize__ for comments
            if obj is None: return
            self.info = getattr(obj, 'info', None)

        def invert(self):
            self *= -1

        def substract_constant(self,c):
            self -= c
        def substract_mean(self,start_x=None,end_x=None):
            if start_x == None: start_x = 0
            if end_x == None: end_x = len(self)
            self -= np.mean(self[start_x:end_x])
        def invert(self):
            self *= -1
        def norm_to_max(self):
            self /= np.max(self)
        def norm_to_constant(self,c):
            self /= c

    def select_by_data_points(self,from_point=0,to_point=None):
        """
        Select data from point to point
        """
        if to_point == None : to_point = self._datalength
        self.current = self.current[from_point:to_point]

    @property
    def filename_prefix(self):
        return self.filename.split('.')[0]


    def file_name_is(self,name):
        return self.file_nameprefix == name

    def header_is(self,header):
        return self.file_nameprefix == header

    # def import_data(self,delimiter,comment_char='#'):
    #     """
    #     parse the CSV data file.
    #     it will be stored in self.data and it is going to be a list of lists [[],[],...]
    #     :param delimiter:
    #     :param quotechar:
    #     :return: non
    #     """
    #     headers, data = parseDatafile(self.fullpath, delimiter, comment_char=comment_char, cast='float')
    #     self.data = np.array(data)

    def set_current(self,n=0, step=1, mean_between_steps=False):
        self.current_n = n
        self.current_step = step
        self.mean_between_steps = mean_between_steps
        self.header = self.experiment_file.column_headers[n]
        """
        set the y data to the nth in datafile, with step as point density
        :param n:
        :param step:
        :return: none
        """

        data = self.experiment_file.column_data[n]
        data_length = len(data)


        # if interpolation is requested
        if mean_between_steps:
            current = np.empty(data_length/step)
            for i,el in enumerate(range(step,data_length,step)) :
                # average each "step" data
                # so that y[y] = average of data from y*step to (y+1)*step
                temp = data[el-step:el]
                current[i] = np.mean(temp)
        else :
            current = data[::step]

        self.current = self.Current(np.array(current))

     # TODO : implement butterworth filtering (from scipy.signal import butter, lfilter)


def firstn(n):
    num = 0
    while num < n:
        yield num
        num += 1







if __name__ == '__main__':
    from matplotlib.pyplot import plot,show
    from OpsinExperiment import OpsinExperiment
    from ExperimentCollection import ExperimentCollection

    # # test opsineExperiment
    # folder = "../experimental/CoChr_powerCurve"
    # filename = "45.txt"
    # e = opsinExperiment(0,1,folder,filename)
    # e.invert_data()
    # e.set_current(0,100,mean_between_steps=True)
    # e.y.substract_mean()
    # plot(e.current_time_space,e.y)
    # e.set_current(0,100,mean_between_steps=False)
    # e.y.substract_mean()
    #
    # plot(e.current_time_space,e.y)
    # show()

    # # test experimentalFolder
    # f = experimentalFolder("/home/marco/Desktop/_marco/ospin_model/experimental/CoChr_powerCurve")
    # print f.file_names_as_string_list
    # for el in f.list_folder_generator():
    #     print el
    # f.set_selection_without_extensions(['10','11'])
    # print f.selection

    # test experimentCollection
    col = ExperimentCollection()
    folder = "/home/marco/Desktop/_marco/ospin_model/experimental/CoChr_powerCurve"
    filename = "45.txt"
    efile1= ExperimentFile(folder,filename,first_line_is_column_headers=False)
    e1 = OpsinExperiment(efile1,0,1)
    folder = "/home/marco/Desktop/_marco/ospin_model/experimental/CoChr_powerCurve"
    filename = "50.txt"
    efile2= ExperimentFile(folder,filename)
    e2 = OpsinExperiment(efile2,0,1)
    col.append(e1)
    col.append(e2)
    #col.pop_by_name('50')
    print col.exp_filename_prefixes
    print col.find_exp_by_filename_prefix('50').filename_prefix
    #
    # print col.find_exp_by_name('50').y
    #
    # print col.shortest_experiment_length
    # col.write_to_CSV('write_to_csv_test.csv')