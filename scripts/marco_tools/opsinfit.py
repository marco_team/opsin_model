__author__ = 'marco'

from scipy.optimize import curve_fit
import numpy as np
from OpsinExperiment import OpsinExperiment
import logging as lg

class OpsinFit:
    def __init__(self,exp):
        # t fit start
        assert (isinstance(exp,OpsinExperiment))
        self.e = exp
        self.data = exp.y
        self.domain = exp.current_time_space
        self.admissible_fit_types = {'tau_on','tau_off','max_current'}

        props = {
            "start_fit_time"    :   None,
            "start_fit_index"   :   None,
            "end_fit_time"      :   None,
            "end_fit_index"     :   None,
            "popt"              :   None,
            "pcov"              :   None,
            "fit_funcion"       :   None,
        }

        self._parameters = dict(zip(self.admissible_fit_types,[dict(props),dict(props),dict(props)]))

        # self._tau_off_fit_properties = dict(props)
        # self._tau_on_fit_properties = dict(props)
        # self._max_current_fit_properties = dict(props)


    def fit_bnds(self,type):
        """
        Return the fit bonds as a touple of coordinates, in a convenient format for plotting.
        ex. plt.plot(f.fit_bnds[0],f.fit_bnds[1],'ro')
        :param type:
        :return:
        """
        assert(type in self.admissible_fit_types)
        temp = self._parameters[type]
        start = temp["start_fit_index"]
        end = temp["end_fit_index"]
        if any(x == None for x in temp):
            raise RuntimeError("fit not yet executed")
        else:
            return (self.e.x[start],self.e.x[end]),\
                   (self.e.y[start],self.e.y[end])


    def f_tau_on(self,x,t,tstart=None):
        """
        Function for fitting tau ON
        :param x:
        :param t:
        :param tstart:
        :return:
        """
        ts = self._parameters["tau_on"]["start_fit_time"]
        # set the max_current value
        a0 = self._parameters["max_current"]["popt"][0]
        if (a0 == None): a0 = 1
        # a0=1

        return  a0*(-1 * np.exp(-1.0/t*(x - ts)) + 1)

    def f_max_current(self,x,a2,a0,k,tstart=None):
        """
        function for fitting the max y value.
        :param x:
        :param a2:
        :param a0:
        :param t:
        :param tstart:
        :return:
        """
        ts = self._parameters["max_current"]["start_fit_time"]
        return -a0 * np.exp(-k*(x - ts)) + a2

#TODO this function does not work how it should when fitting
# se sostituisci il valore di self._t_fit_start nel return statement, funziona

    def f_tau_off(self,x,t,a0):
        """
        Function for fitting tau OFF (decaying exponential)
        a0 * np.exp(-t*(y - ts))
        :param x:
        :param t:
        :param a0:
        :return:
        """
        ts = self._parameters["tau_off"]["start_fit_time"]
        return a0 * np.exp(-1.0/t*(x - ts))

    def fit_parameter(self, type=None, start_fit_time=None,end_fit_time=None,
                      start_fit_index = None, end_fit_index = None, percet_of_max_current=50, p0=None):
        """
        Fit the requested parameters.
        :param type: The fit type: it can be 'tau_on', 'tau_off' or 'max_current'
        :param start_fit_time:
        :param end_fit_time:
        :param start_fit_index:
        :param end_fit_index:
        :param percet_of_max_current:
        :return:
        """

        assert(type in self.admissible_fit_types)
        out_record = self._parameters[type]
        # p0 = None

        if type == "tau_on" or type == "max_current":
            start = self.e.pulse_start
            find_start = self.e.pulse_start_find
            fitting_tau_on = True

            # chose the fitting function, depending on the requested fit
            if type == "tau_on" :
                out_record["fit_funcion"] = self.f_tau_on
                # p0 = None
            elif type == "max_current":
                out_record["fit_funcion"] = self.f_max_current
                # p0 = None

        else:  # iftype == "tau_off":
            start = self.e.pulse_end
            find_start = self.e.pulse_end_find
            fitting_tau_on = False
            out_record["fit_funcion"] = self.f_tau_off
            # p0 = None

        # default value for START FIT is the beginning of the pulse
        if start_fit_time == None and start_fit_index == None:
            if start != None:
                start_fit_time = start
                start_fit_index = self.find_index_from_time(start_fit_time)
            else:
                start_fit_time, start_fit_index = find_start()
        elif start_fit_index != None:
            start_fit_time = self.domain[start_fit_index]
        elif start_fit_time != None:
            start_fit_index = self.find_index_from_time(start_fit_time)

        # default time interval is tau_off/2 otherwise 50% of max y
        if end_fit_time == None and end_fit_index == None:
            if fitting_tau_on and self.tau_off != None:
                end_fit_time = start_fit_time + self.tau_off/2
            elif fitting_tau_on:
                end_fit_time = self.find_time_at_max_percentage(percet_of_max_current)
                end_fit_index = self.find_index_from_time(end_fit_time)
            elif not fitting_tau_on:
                end_fit_index = len(self.e.x)-1
                end_fit_time = self.e.x[end_fit_index]
        elif end_fit_index != None:
            end_fit_time = self.domain[end_fit_index]
        elif end_fit_time != None:
            end_fit_index = self.find_index_from_time(end_fit_time)

        #out_record["t_fit_start"] = self.e.x[start_fit_index]

        # print "t fit start", self._t_fit_start
        data = self.e.y[start_fit_index:end_fit_index]
        timespace = self.e.x[start_fit_index:end_fit_index]
        sigma = self.e.sigma[start_fit_index:end_fit_index]

        out_record["start_fit_time"] = start_fit_time
        out_record["start_fit_index"] = start_fit_index
        out_record["end_fit_time"] = end_fit_time
        out_record["end_fit_index"] = end_fit_index

        # FIT
        try:
            popt, pcov = curve_fit(
                out_record["fit_funcion"],  # fit function
                timespace,         # y values
                data,              # data to fit
                p0     # initial guess for the parameters
                #sigma = sigma      # weights (see documentation)
            )
        except RuntimeError:
            print "error while fitting"
            popt = [0,0.1,0]
            pcov = None




        out_record["popt"] = popt
        out_record["pcov"] = pcov

    @property
    def tau_on(self):
        d = self._parameters['tau_on']
        return d["popt"][0]
    @property
    def tau_off(self):
        d = self._parameters['tau_off']
        return d["popt"][0]
    @property
    def max_current(self):
        d = self._parameters['max_current']
        return d["popt"][0]

    def model(self, fit_type):
        assert(fit_type in self.admissible_fit_types)
        d = self._parameters[fit_type]
        prms = d["popt"]
        start = d["start_fit_index"]
        end = d["end_fit_index"]
        if fit_type != "tau_off":
            # end,temptime = self.e.pulse_end_find()
            temptime,end = self.e.pulse_end_find()

        timespace = self.e.x[start:]
        return timespace, d["fit_funcion"](timespace,*prms)


    def find_pulse_start(self):
        pass

    def find_time_at_max_percentage(self,p,reversed=False):
        m = np.max(self.data)*p*1.0/100
        temp = zip(self.domain,self.data)
        t = None
        if reversed: temp = temp[::-1]
        for t in enumerate(temp):
            if t[1][1] > m: break
        return t[1][0]

    def find_index_from_time(self,time):
        # return the index in the domain which is closest to time
        temp = np.abs(self.domain-time)
        return np.argmin(temp)





def test1() :
    # LOAD DATA
    folder = "../../experimental/CoChr_powerCurve/"
    folder = ExperimentFolder(folder)
    filename = folder.file_names_as_string_list[2]
    efile = ExperimentFile(folder.folder, filename)
    # create a new Opsin Experiment
    e = OpsinExperiment(efile.column_data[0], tstart=0, tend=1000)

    # DATA PREPARATION
    # chose the y experiment and the sampling step
    e.set_data_density(50, mean_between_steps=True)
    # substract the base y value
    e.y.substract_mean(600,len(e.y))
    # invert the y
    e.y.invert()
    print len(e.x), len(e.y)

    plt.plot(e.x,e.y,'.')

    f = OpsinFit(e)
    # first fit for normalization
    f.fit_parameter("max_current",end_fit_time=170)

    b = f.fit_bnds("max_current")


    # Plot max y fit
    result = f.model("max_current")

    # fit tau off
    f.fit_parameter("tau_off")
    f.fit_parameter("tau_on",end_fit_time=170)

    # get fitting boudaries for later plotting
    boff = f.fit_bnds("tau_off")
    bon = f.fit_bnds("tau_on")

    # plot data and fit

    plt.plot(boff[0],boff[1],'yo')
    plt.plot(bon[0],bon[1],'ro')

    plt.plot(f.model("tau_on")[0],f.model("tau_on")[1])
    plt.plot(f.model("tau_off")[0],f.model("tau_off")[1])

    plt.axis([0,1000,0,90])

    plt.show()

    print f.tau_off

    print "end fit"


def test2():
    # ExperimentFolder is an object that looks for experiment files in a given folder
    folder = ExperimentFolder("../../experimental/")
    filename = folder.file_names_as_string_list[2]

    # ExperimentFile is an object that loads data from a CSV file
    efile = ExperimentFile(folder.folder, filename)
    cd = efile.column_data[0]

    e = OpsinExperiment(cd, tstart=0, tend=500)
    # invert the data (current is now positive)
    e.y.invert()


    e.set_data_density(3, mean_between_steps=True)



    # set the laser pulse start and end time
    e.pulse_start = 116
    e.pulse_end = 0.0836012861736334
    # e.select_by_data_points(0,-10)

    print e.t_start, e.t_end, e.dt

    plt.plot(e.x, e.y,'.')

    fit = OpsinFit(e)
    fit.fit_parameter("max_current", start_fit_time=115.5, end_fit_time=115.5+4)

    fit_result = fit.model("max_current")
    bnd = fit.fit_bnds("max_current")
    norm_param = fit.max_current

    plt.plot(np.linspace(0,500,100),[norm_param]*100,'--',label="max current in a two level system = {}".format(int(round(norm_param))));
    plt.plot(fit_result[0],fit_result[1],'k--',label="exponential fit of the highest power curve")
    plt.plot(bnd[0],bnd[1],'ko')

    plt.legend()

    plt.show()



def test3():
    fn = ("/home/marco/Desktop/ospin_model/opsin_model/experimental/CoChR Valeria/DAtaCochR_Valeria/txt/lun2nov2015c1/temp/056.txt")
    ydata = np.loadtxt(fn)
    xdata = np.linspace(0,1,len(ydata))
    e = OpsinExperiment(ydata, xdata=xdata)
    e.label = fn.split('.')[0]

    e.select_by_time(0,0.4)
    e.y.substract_constant(np.mean(e.y[-500:]))
    e.y.invert()
    e.change_x_reference(0.1+0.001)
    e.pulse_start = 0
    e.update_raw_data_from_current()

    fit = OpsinFit(e)
    fit.fit_parameter("max_current", start_fit_time=-0.00, end_fit_time=0.01)
    fit_result = fit.model("max_current")
    bnd = fit.fit_bnds("max_current")
    norm_param = fit.max_current

    e.plot('.')

    plt.plot(fit_result[0],fit_result[1],'r--',label="exponential fit of the highest power curve")
    plt.plot(bnd[0],bnd[1],'ro')


    plt.legend();
    plt.ylim([-100,2000])
    plt.xlim([-0.1,0.4])

    plt.show()


if __name__ == "__main__":
    from ExperimentFile import ExperimentFile
    from ExperimentFolder import ExperimentFolder
    import matplotlib.pyplot as plt

    logger = lg.getLogger()
    logger.setLevel(lg.INFO)


    # test1()
    # test2()
    test3()