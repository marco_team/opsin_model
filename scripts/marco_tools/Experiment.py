__author__ = 'marco'

from parsedatafile import parse_datafile
from listToCSV import list_to_CSV
import numpy as np
import os
from ExperimentFolder import ExperimentFolder
from ExperimentFile import ExperimentFile
import quantities as pq
import matplotlib.pyplot as plt


class Experiment(object):
    """
    an experiment has a data file (CSV) associated with
    can parse the data file
    """

    def __init__(self, ydata, xdata=None, label=None, comment=None, sigma=None):
        """
        experiment_file is an object of type experimentFile
        """

        ### COPY INPUT DATA
        if xdata == None:
            xdata = np.arange(len(ydata))

        self._xdata = self.DataArray(xdata);
        self._ydata = self.DataArray(ydata);

        ### VECTOR OF MEASURE UNCERTAINTIES
        ### incertitudes on data are 1 for each point if not specified
        if sigma == None:
            self._sigma = np.ones(len(self._ydata))

        # self.y = self.DataArray(ydata)
        # self.x = self.DataArray(xdata)
        # stores of resampled data

        ### DATA SELECTION (in x units)
        self.selection = (0,self._xdata[-1])

        ### RESAMPLED VECTORS (initially equal to the raw data)
        self._resampled_data_x = self.DataArray(self._xdata)
        self._resampled_data_y = self.DataArray(self._ydata)
        self._resampled_sigma  = self.DataArray(self._sigma)

        self.label = label  # the header of this experiment
        self.comment = comment

        ### if the x and y vectors are quantities, save the dimentionality information
        if isinstance(xdata,pq.Quantity):
            self._x_units = xdata.dimensionality
        else:
            self._x_units = pq.dimensionless
        if isinstance(ydata,pq.Quantity):
            self._y_units = ydata.dimensionality
        else:
            self._y_units = pq.dimensionless


    def index_of_x(self,t):
        """ Return the index of the element of x which is closest to t"""
        return abs(self._resampled_data_x - t).argmin()

    ### This return the selected data
    @property
    def x(self):
        a,b = self._index_of_selection
        return self._resampled_data_x[a:b]
    @property
    def y(self):
        a,b = self._index_of_selection
        return self._resampled_data_y[a:b]
    @property
    def sigma(self):
        a,b = self._index_of_selection
        return self._resampled_sigma[a:b]
    @property
    def _index_of_selection(self):
        """ Return the index of current selection (the latter is expressed in x_units """
        return (self.index_of_x(self.selection[0]), self.index_of_x(self.selection[1]))


    ### OPERATIONS ON DATA STORES
    def update_raw_data_from_current(self):
        """ Overwrite the raw data with the current x and y vectors """
        temp_x = self.DataArray(self.x)
        temp_y = self.DataArray(self.y)
        self._xdata = self.DataArray(temp_x)
        self._ydata = self.DataArray(temp_y)
        self._resampled_data_x = self.DataArray(temp_x)
        self._resampled_data_y = self.DataArray(temp_y)
        # self.selection = (self.x[0], self.x[-1])
    def reset_current_to_raw_data(self):
        """ Overwrite the current data with the raw data in memory"""
        self._resampled_data_x = self.DataArray(self._xdata)
        self._resampled_data_y = self.DataArray(self._ydata)

    ### UNIT CONVERSIONS
    def _convert_vector(self,v,old_units,new_units):
        """ Overwrite the vector v, scaling it according to the given units """
        temp_q = pq.Quantity(1,old_units)
        temp_q = temp_q.rescale(new_units)
        temp_v = self.DataArray(v*temp_q.magnitude)
        return pq.Quantity(1,new_units).dimensionality, temp_v
    def convert_x(self,units):
        """ Overwrite the resampled and raw x data, scaling it according to the given units """
        old_units = self._x_units
        self._x_units, self._resampled_data_x =  self._convert_vector(
            self._resampled_data_x,
            old_units,
            units)
        self._x_units, self._xdata =  self._convert_vector(
            self._xdata,
            old_units,
            units)
        ### Convert the selection
        temp_q = pq.Quantity(1,old_units)
        temp_q = temp_q.rescale(units)
        self.selection = self.selection*temp_q.magnitude
    def convert_y(self,units):
        """ Overwrite the resampled and raw y data, scaling it according to the given units """
        old_units = self._y_units
        self._y_units, self._resampled_data_y =  self._convert_vector(
            self._resampled_data_y,
            old_units,
            units)
        self._y_units, self._ydata =  self._convert_vector(
            self._ydata,
            old_units,
            units)

    ### DATA SELECTION
    def select_by_time(self, from_time, to_time):
        """ Change START and END selection time """
        # start_index = self.index_of_x(from_time)
        # end_index = self.index_of_x(to_time)
        self.selection = (from_time, to_time)

    ### SMOOTHING
    def smoothing(self, n=2):
        """ Operate a smoothing on the resampled data """
        for i in range(len(self._resampled_data_y)):
            a = max(0, i - n)
            b = min(len(self._resampled_data_y), i + n)
            self._resampled_data_y[i] = np.mean(self._resampled_data_y[a:b])

    def change_x_reference(self,new_zero): ### THIS DOES NOT WORK
        """ Change the zero of the x axis to the given value """
        self._xdata.substract_constant(new_zero)
        self._resampled_data_x.substract_constant(new_zero)
        self.selection = (self.selection[0]-new_zero,self.selection[1]-new_zero)

    ### PLOTTING
    def plot(self, *args, **kwargs):
        plt.plot(self.x,self.y,label=self.label,*args)
        plt.xlabel(self._x_units)
        plt.ylabel(self._y_units)

        try:
            if kwargs["sigma"] :
                plt.plot(self.x, self.y+self.sigma,
                         self.x, self.y-self.sigma)
        except:
            pass

    ### Resample the data
    def resample(self, step):
        """
            Resample the data with the given step.
            The mean value over one step is taken as new data value
            The std over the step is taken as measure uncertainty
        """

        # data = self.ydata
        data = self._resampled_data_y
        data_length = len(data)

        current = np.empty(data_length / step)
        current_sigma = np.empty(data_length / step)
        temp_x = np.empty(data_length / step)

        for i, el in enumerate(range(step, data_length, step)):
            # average each "step" data
            # so that y[y] = average of data from y*step to (y+1)*step
            temp = data[el - step:el]
            current[i] = np.mean(temp)
            current_sigma[i] = np.std(temp)
            temp_x[i] = self._resampled_data_x[el - step]

        self._resampled_data_y = self.DataArray(current)
        self._resampled_data_x = self.DataArray(temp_x)
        self._resampled_sigma  = self.DataArray(current_sigma)

    class DataArray(np.ndarray):
    # class DataArray(pq.Quantity):
        def __new__(cls, input_array, info=None):
            # Input array is an already formed ndarray instance
            # We first cast to be our class type
            # obj = np.asarray(input_array).view(cls)
            obj = np.asarray(np.copy(input_array)).view(cls)
            # add the new attribute to the created instance
            obj.info = info
            # Finally, we must return the newly created object:
            return obj

        def index_of_x(self,t):
            """ Return the index of the element of x which is closest to t"""
            return abs(self - t).argmin()

        def __array_finalize__(self, obj):
            # see InfoArray.__array_finalize__ for comments
            if obj is None: return
            self.info = getattr(obj, 'info', None)

        def invert(self):
            self *= -1

        def substract_constant(self, c):
            self -= c

        def substract_min(self):
            self -= np.min(self)

        def substract_mean(self, start_x=None, end_x=None):
            # TODO :: CHECK THIS FUNCTION
            if start_x == None: start_x = 0
            else: start_x = self.index_of_x(start_x)

            if end_x == None: end_x = len(self)
            else: end_x = self.index_of_x(end_x)

            self -= np.mean(self[start_x:end_x])

        def invert(self):
            self *= -1

        def norm_to_max(self):
            self /= np.max(self)

        def norm_to_constant(self, c):
            self /= c




if __name__ == '__main__':
    from matplotlib.pyplot import plot, show
    from ExperimentFolder import ExperimentFolder
    from ExperimentFile import ExperimentFile

    print "testing experimentfile"

    folder = ExperimentFolder("../../experimental/")
    filename = folder.file_names_as_string_list[2]
    print filename

    # ExperimentFile is an object that loads data from a CSV file
    efile = ExperimentFile(folder.folder, filename)

    ydata = pq.Quantity(efile.column_data[0],'mA')
    xdata = pq.Quantity(range(len(ydata)),'ms')

    e = Experiment(ydata,xdata=xdata)
    e.convert_x('s')
    e.label = "experiment"


    e.plot()
    e.select_by_time(1,3)
    e.plot()
    e.update_raw_data_from_current()
    e.plot()
    # plt.plot(e._xdata,e._ydata)
    # print len(e._xdata),len(e._ydata)
    # print len(e.x),len(e.y)


    # e.reset_to_raw_data()
    # e.convert_x('s')
    # plot(e.x, e.y,'r')

    # e.set_data_density(50, mean_between_steps=True)
    # e.select_by_data_points(from_point=10, to_point=300)

    # e.select_by_time(1,3)
    # e.change_x_reference(1)
    # e.change_x_reference(-1)
    # e.select_by_time(0,4)

    #

    #
    # e.plot(sigma=True)
    # plt.plot(e.resampled_data_x,e.resampled_data_y,'k')
    # plot(e.x, e.y)
    # plot(e.x, e.y + e.sigma)
    # plot(e.x, e.y - e.sigma)
    mp_index_max = len(timespace)-1
    # plt.legend()
    show()

