__author__ = 'marco'

from Experiment import Experiment
import numpy as np

class OpsinExperiment(Experiment):
    def __init__(self, ydata, xdata=None, label=None, comment=None, tstart=None, tend=None):

        self.light_power = None
        self.pulse_start = None
        self.pulse_end = None
        self._dt = None
        self._t_start = tstart
        self._t_end = tend


        super(OpsinExperiment, self).__init__(ydata, xdata=xdata, label=label, comment=comment)

        if (tstart != None) and (tend != None):
            # create a new time space with the proper given limits
            temp = np.linspace(tstart,tend,len(self.y))
            self.x = self.DataArray(temp)
        else:
            self._t_start = self.x[0]
            self._t_end = self.x[-1]


    @property
    def current_time_space(self):
        return self.x

    @property
    def dt(self):
        return self.x[1] - self.x[0]

    @property
    def t_start(self):
        return self.x[0]

    @property
    def t_end(self):
        return self.x[-1]

    def set_data_density(self, step , mean_between_steps=False):
        super(OpsinExperiment, self).set_data_density(step,mean_between_steps)

    def pulse_start_find(self):
        """
        Try to find the pulse starts time, as the last point where the y is zero before the peak.
        :return:
        """
        temp_max = np.max(self.y)
        beyond_max = False
        rev = self.y[::-1]
        for i,x in enumerate(rev):
            if not beyond_max:
                if x == temp_max : beyond_max = True
                continue
            elif x < 0 :
                #i = i + 1
                break
        i = len(self.current_time_space) - i # the list was reversed
        return self.current_time_space[i] , i
    
    def pulse_end_find(self):
        """
        Try to find the time the pulse ends.
        :return:
        """
        old_x = self.y[-1]
        m = np.max(self.y)/2
        for i,x in enumerate(reversed(self.y)):
            if x<m:
                continue
            if x < old_x:
                i = len(self.current_time_space) - i
                break
            old_x = x

        return self.current_time_space[i], i


    @property
    def CSV_column_headers(self,delimiter='\t'):
        return delimiter.join(['file_name','header','comment','t_start','t_end','pulse_start','pulse_end','current_vs_time'])

    def data_to_string(self):
        return ','.join([str(num) for num in self.y[::] ] )

    def to_CSV_line(self,delimiter='\t'):
        s = delimiter.join([self.filename,
                       self.header,
                       self.comment,
                       str(self.t_start),
                       str(self.t_end),
                       str(self.pulse_start),
                       str(self.pulse_end),
                       self.data_to_string()])
        return s