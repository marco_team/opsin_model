__author__ = 'marco'

import Experiment as exp
from listToCSV import list_to_CSV
import numpy as np
from ExperimentFile import ExperimentFile
from OpsinExperiment import OpsinExperiment

class ExperimentCollection(list):
    # a collection of experiments, is a list

    def __init__(self):
        self._x_axis_domain = None

    def __getitem__(self, key):
        # get an experiment in the collection
        return super(ExperimentCollection, self).__getitem__(key)

    def __setitem__(self, key, value):
        # set an experiment to the collection
        print "ok"
        if self.check_instance(value):
            super(ExperimentCollection, self).__setitem__(key,value)
        else:
            raise Exception('experimentCollection only contains object of class experiment')

    def append(self, p_object):
        # append an experiment in the collection
        if self.check_instance(p_object):
            super(ExperimentCollection, self).append(p_object)
        else:
            raise Exception('experimentCollection only contains object of class experiment')

    def check_instance(self,e):
        """
        check if the type of e is experiment
        :param e:
        :return:
        """
        return isinstance(e,exp.Experiment)

    @property
    def x_axis_domain(self):
        return self._x_axis_domain
    @x_axis_domain.setter
    def x_axis_domain(self,list_of_x_axis_values):
        # TODO : check if all experiments have the same length ?
        self._x_axis_domain = list_of_x_axis_values

    @property
    def exp_filename_prefixes(self):
        names = []
        for e in self:
            names.append(e.filename_prefix)
        return names

    def find_exp_by_filename_prefix(self,name):
        # return the first experiment with name equal to name
        i = self.exp_filename_prefixes.index(name)
        return self[i]

    def pop_by_filename_prefix(self,name):
        i = self.exp_filename_prefixes.index(name)
        self.pop(i)

    def get_selection(self, namelist):
        # return a sub-collection of expermients, which the user can construct by name
        templist = ExperimentCollection()
        for e in self:
            if e.name in namelist:
                templist.append(e)
        return templist

    @property
    def shortest_experiment_length(self):
        #find the lenght of the shortest experiment
        shortest = len(self[0].current)
        for e in self:
            if len(e.current) < shortest:
                shortest = len(e.current)
        return shortest

    def write_to_CSV_in_columns(self,filename):
        # write this collection as CSV file (traditional format, data are in columns
            data_list = []
            name_list = []
            if self.x_axis_domain == None:
                self.x_axis_domain = np.array(range(self.shortest_experiment_length))

            data_list.append(self.x_axis_domain.tolist()) # y is a numpy array
            name_list.append('y-axis')

            for e in self:
                # add to the data_list the y experiment.
                # all data are truncated to the length of the shortest
                data_list.append(e.current.tolist()[0:self.shortest_experiment_length])
                name_list.append(e.filename_prefix)

            return list_to_CSV(data_list,filename,column_titles=name_list)

    def write_to_CSV_in_lines(self,filename):
        # write this collection as CSV file (data are in lines, requires tratement for extraction)
        print 'writing...'
        with open(filename,'wb') as f:
            f.write('{}\n'.format(self[0].CSV_column_headers))
            for e in self:
                f.write('{}\n'.format(e.to_CSV_line()))


    # TODO this method
    def populate_opsinExperiments_from_CSV(self,foldername,filename):
        # return a new collection with values from CSV
        pass



if __name__ == '__main__':
    from matplotlib.pyplot import plot,show

    # test experimentCollection
    col = ExperimentCollection()
    folder = "/home/marco/Desktop/_marco/ospin_model/experimental/CoChr_powerCurve"
    filename = "45.txt"
    efile1= ExperimentFile(folder,filename,first_line_is_column_headers=False)
    e1 = OpsinExperiment(efile1,0,1)
    folder = "/home/marco/Desktop/_marco/ospin_model/experimental/CoChr_powerCurve"
    filename = "50.txt"
    efile2= ExperimentFile(folder,filename,first_line_is_column_headers=False)
    e2 = OpsinExperiment(efile2,0,1)
    col.append(e1)
    col.append(e2)
    #col.pop_by_name('50')
    print col.exp_filename_prefixes
    print col.find_exp_by_filename_prefix('50').filename_prefix

    #
    # print col.find_exp_by_name('50').y
    #
    # print col.shortest_experiment_length
    # col.write_to_CSV('write_to_csv_test.csv')