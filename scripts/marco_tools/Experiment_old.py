__author__ = 'marco'

from parsedatafile import parse_datafile
from listToCSV import list_to_CSV
import numpy as np
import os
from ExperimentFolder import ExperimentFolder
from ExperimentFile import ExperimentFile
import quantities as pq
import matplotlib.pyplot as plt


class Experiment(object):
    """
    an experiment has a data file (CSV) associated with
    can parse the data file
    """

    def __init__(self, ydata, xdata=None, label=None, comment=None, sigma=None):
        """
        experiment_file is an object of type experimentFile
        """
        if xdata == None:
            xdata = np.arange(len(ydata))

        self.xdata = np.copy(xdata);
        self.ydata = np.copy(ydata);
        self.y = self.DataArray(ydata)
        self.x = self.DataArray(xdata)
        self.label = label  # the header of this experiment
        self.comment = comment
        self.current_step = 1
        self.mean_between_step = False

        ### if the x and y vectors are quantities, save the unit information
        if isinstance(xdata,pq.Quantity):
            self.x_units = xdata.dimensionality
        else:
            self.x_units = pq.dimensionless
        if isinstance(ydata,pq.Quantity):
            self.y_units = ydata.dimensionality
        else:
            self.y_units = pq.dimensionless

        # incertitudes on data are 1 for each point if not specified
        if sigma == None:
            self.sigma = np.ones(len(self.y))

        # stores of resampled data
        self.resampled_data_x = self.DataArray(self.x)
        self.resampled_data_y = self.DataArray(self.y)
        self.resampled_sigma = self.DataArray(self.sigma)

    def convert_x(self,units):
        temp_q = pq.Quantity(1,self.x_units)
        temp_q = temp_q.rescale(units)
        # print temp_q
        self.x_units = pq.Quantity(1,units).dimensionality
        self.x = self.DataArray(self.x*temp_q.magnitude)
        self.resampled_data_x = self.DataArray(self.resampled_data_x*temp_q.magnitude)
        # self.resampled_data_x = self.DataArray(self.x)

    def convert_y(self,units):
        temp_q = pq.Quantity(1,self.x_units)
        temp_q = temp_q.rescale(units)
        # print temp_q
        self.x_units = pq.Quantity(1,units).dimensionality
        self.x = self.DataArray(self.x*temp_q.magnitude)
        self.resampled_data_y = self.DataArray(self.resampled_data_y*temp_q.magnitude)
        # self.resampled_data_y = self.DataArray(self.y)


    class DataArray(np.ndarray):
    # class DataArray(pq.Quantity):
        def __new__(cls, input_array, info=None):
            # Input array is an already formed ndarray instance
            # We first cast to be our class type
            # obj = np.asarray(input_array).view(cls)
            obj = np.asarray(input_array).view(cls)
            # add the new attribute to the created instance
            obj.info = info
            # Finally, we must return the newly created object:
            return obj

        def __array_finalize__(self, obj):
            # see InfoArray.__array_finalize__ for comments
            if obj is None: return
            self.info = getattr(obj, 'info', None)

        def invert(self):
            self *= -1

        def substract_constant(self, c):
            self -= c

        def substract_min(self):
            self -= np.min(self)

        def substract_mean(self, start_x=None, end_x=None):
            if start_x == None: start_x = 0
            if end_x == None: end_x = len(self)
            self -= np.mean(self[start_x:end_x])

        def invert(self):
            self *= -1

        def norm_to_max(self):
            self /= np.max(self)

        def norm_to_constant(self, c):
            self /= c

    def _change_data_range(self, from_point, to_point):
        self.y = self.DataArray(self.resampled_data_y[from_point:to_point])
        self.x = self.DataArray(self.resampled_data_x[from_point:to_point])
        self.sigma = self.DataArray(self.resampled_sigma[from_point:to_point])

    def select_by_data_points(self, from_point=0, to_point=None):
        if to_point == None: to_point = len(self.y)  # maybe ERROR here
        self._change_data_range(from_point, to_point)

    def select_by_time(self, from_time, to_time):
        start_index = abs(self.resampled_data_x-from_time).argmin()
        end_index = abs(self.resampled_data_x-to_time).argmin()
        self._change_data_range(start_index,end_index)

    def label_is(self, label):
        return self.label == label

    def smoothing(self, n=2):
        ### LO SMOOTHING NON MODIFICA IL resampled_data array
        for i in range(len(self.y)):
            a = max(0, i - n)
            b = min(len(self.y), i + n)
            self.y[i] = np.mean(self.y[a:b])

        # for i in range(len(self.y)):
        #     a = max(0, i - n)
        #     b = min(len(self.y), i + n)
        #     self.resampled_data_y[i] = np.mean(self.resampled_data_y[a:b])

        # self.resampled_data_y = self.DataArray(self.y)

    # def change_x_reference(self,new_zero): ### THIS DOES NOT WORK
    #     self.x.substract_constant(new_zero)
    #     self.resampled_data_x.substract_constant(new_zero)

    def set_data_density(self, step, mean_between_steps=False):
        """
        set the y data to the nth in datafile, with step as point density
        :param n:
        :param step:
        :return: none
        """
        self.current_step = step
        self.mean_between_steps = mean_between_steps

        # data = self.ydata
        data = self.resampled_data_y
        data_length = len(data)


        # if interpolation is requested
        if mean_between_steps:
            current = np.empty(data_length / step)
            current_sigma = np.empty(data_length / step)
            temp_x = np.empty(data_length / step)

            for i, el in enumerate(range(step, data_length, step)):
                # average each "step" data
                # so that y[y] = average of data from y*step to (y+1)*step
                temp = data[el - step:el]
                current[i] = np.mean(temp)
                current_sigma[i] = np.std(temp)
                temp_x[i] = self.x[el - step]

        else:
            current = data[::step]
            temp_x = self.x[::step]



        self.y = self.DataArray(np.array(current))
        self.x = self.DataArray(temp_x)
        self.sigma = current_sigma

        self.resampled_data_x = self.DataArray(self.x)
        self.resampled_data_y = self.DataArray(self.y)
        self.resampled_sigma = self.DataArray(self.sigma)


    def index_of_x(self,x):
        return abs(self.x-x).argmin()

    def plot(self, *args, **kwargs):
        plt.plot(self.x,self.y,label=self.label,*args)
        plt.xlabel(self.x_units)
        plt.ylabel(self.y_units)

        try:
            if kwargs["sigma"] :
                plt.plot(self.x, self.y+self.sigma,
                         self.x, self.y-self.sigma)
        except:
            pass


# def firstn(n):
#     num = 0
#     while num < n:
#         yield num
#         num += 1
#
#
#
#



if __name__ == '__main__':
    from matplotlib.pyplot import plot, show
    from ExperimentFolder import ExperimentFolder
    from ExperimentFile import ExperimentFile

    print "testing experimentfile"

    folder = ExperimentFolder("../../experimental/")
    filename = folder.file_names_as_string_list[2]
    print filename

    # ExperimentFile is an object that loads data from a CSV file
    efile = ExperimentFile(folder.folder, filename)

    ydata = pq.Quantity(efile.column_data[0],'mA')
    xdata = pq.Quantity(range(len(ydata)),'ms')

    e = Experiment(ydata,xdata=xdata)
    e.convert_x('s')
    e.label = "experiment"

    plot(e.x, e.y)
    e.set_data_density(50, mean_between_steps=True)
    # e.select_by_data_points(from_point=10, to_point=300)

    e.select_by_time(1,3)
    # e.change_x_reference(1)
    # e.change_x_reference(-1)
    # e.select_by_time(0,4)

    #

    #
    e.plot(sigma=True)
    # plt.plot(e.resampled_data_x,e.resampled_data_y,'k')
    # plot(e.x, e.y)
    # plot(e.x, e.y + e.sigma)
    # plot(e.x, e.y - e.sigma)

    plt.legend()
    show()

