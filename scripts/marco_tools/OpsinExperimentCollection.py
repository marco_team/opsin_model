__author__ = 'marco'

import sys
sys.path.append('..')


from sqliteutils import *
from ExperimentCollection import ExperimentCollection
from OpsinExperiment import OpsinExperiment

class opsinExperimentCollection(ExperimentCollection):
    def __init__(self,folder='.',name='experimentCollection'):
        self.columns = [
            'Name STRING',
            'T_start DOUBLE',
            'T_end DOUBLE',
            'Pulse_start DOUBLE',
            'Pulse_end DOUBLE',
            'Lightpower DOUBLE',
            'Time ARRAY',
            'Current ARRAY'
        ]
        self.folder = '.'
        self.name = name
        super(opsinExperimentCollection, self).__init__()

    def to_list(self,e):
        l = (
            e.name,
            e.t_start,
            e.t_end,
            e.pulse_start,
            e.pulse_end,
            e.light_power,
            e.current_time_space,
            e.current
        )

    @property
    def filename(self):
        return "{}/{}.db".format(self.folder,self.name)

    def create_SQLite(self):
        cur = create_db_with_one_table(self.filename,'Experiments',self.columns)
        for e in self:
            cur.execute('insert into Experiments VALUES(?, ?, ?, ?, ?, ?, ?, ?);', self.to_list(e))


if __name__ == '__main__':
    from matplotlib.pyplot import plot,show

    # test experimentCollection
    col = opsinExperimentCollection()
    folder = "/home/marco/Desktop/_marco/ospin_model/experimental/CoChr_powerCurve"
    filename = "45.txt"
    e1 = OpsinExperiment(0,1,folder,filename)
    folder = "/home/marco/Desktop/_marco/ospin_model/experimental/CoChr_powerCurve"
    filename = "50.txt"
    e2 = OpsinExperiment(0,1,folder,filename)
    col.append(e1)
    col.append(e2)
    #col.pop_by_name('50')
    print col.exp_names
    print col.find_exp_by_name('50').name

    register_adapters_numpy_array_in_sqlite3()
    col.create_SQLite()
    #
    # print col.find_exp_by_name('50').y
    #
    # print col.shortest_experiment_length
    # col.write_to_CSV('write_to_csv_test.csv')