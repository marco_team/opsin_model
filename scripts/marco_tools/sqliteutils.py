import sqlite3
import numpy as np
import io
import sys

__author__ = 'marco'

def connect_db(dbase):
    """
    Open database dbase
    """
    try:
        connected_database = sqlite3.connect(dbase, detect_types=sqlite3.PARSE_DECLTYPES)  # connect to DB
    except sqlite3.Error, e:
        print "Error %s:" % e.args[0]
        sys.exit(1)
    return connected_database

def read_table(dbase,table_name):
    """
    Read table in a dbase
    """
    con = connect_db(dbase)
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    cur.execute( "SELECT * FROM {}".format(table_name) )
    tab = cur.fetchall()

    return tab


def create_db_with_one_table(db,table_name, column_list):
    """
    creates a test DB for extracting statistics
    """

    # connect database
    con = connect_db(db)
    cur = con.cursor()

    # create
    columns = ', '.join(column_list)

    # Create Coordinate table
    cur.execute("DROP TABLE IF EXISTS {}".format(table_name))
    command = "CREATE TABLE {0}({1})".format(table_name,columns)
    cur.execute(command)

    return cur


def adapt_array(arr):
    """
    http://stackoverflow.com/a/31312102/190597 (SoulNibbler)
    """
    out = io.BytesIO()
    np.save(out, arr)
    out.seek(0)
    return sqlite3.Binary(out.read())

def convert_array(text):
    out = io.BytesIO(text)
    out.seek(0)
    return np.load(out)

def register_adapters_numpy_array_in_sqlite3():
    # Converts np.array to TEXT when inserting
    sqlite3.register_adapter(np.ndarray, adapt_array)
    # Converts TEXT to np.array when selecting
    sqlite3.register_converter("ARRAY", convert_array)


register_adapters_numpy_array_in_sqlite3()

if __name__ == "__main__":
    # con = sqlite3.connect(":memory:", detect_types=sqlite3.PARSE_DECLTYPES)
    # cur = con.cursor()
    # cur.execute("create table test (arr array)")

    columns = [
        'Id INTEGER',
        'Text STRING',
        'arr ARRAY'
    ]

    cur = create_db_with_one_table("test.db",'tabname',columns)
    cur.execute("INSERT INTO Space VALUES(?, ?);", (points, side))

    print read_table("test.db",'tabname')