__author__ = 'marco'

import numpy as np

from igor.packed import load, walk
from igor.record.wave import WaveRecord
from igor.script import Script
import pprint
from igor.script import Script
from os import system

IGOR_PY_MODULE_PATH = '/home/marco/miniconda2/envs/condaPy2/bin/igorpackedexperiment.py'

class IGORExperimentFile:
    """
    import an Igor file
    """

    def __init__(self,file_name):
        # set folder and file names
        self.file_name = file_name

    def import_data(self, block=0, segment=0, signal=0):
        """
        parse the CSV data file.
        """

        # make oult_filename
        temp = self.file_name.split('/')[-1].split('.');
        outfile = '.'.join([temp[0],'txt'])

        #
        # command = 'python {} -f {} -o {}'.format(IGOR_PY_MODULE_PATH, self.file_name, outfile)
        # print command
        # system(command)
        #
        #


        records, filesystem = load(self.file_name)
        print len(records)
        # print pprint.pformat(filesystem)
        print filesystem['root']['K13']
        # print pprint.pformat(records)

        # try:
        #     f = open(outfile, 'r')
        #     print f.readlines()
        # except:
        #     raise
        # return sig.times, sig

if __name__ == "__main__":
    f = IGORExperimentFile("../../experimental/igor_test/mar25nov2014_cella.pxp")
    f.import_data()

#     import matplotlib.pyplot as plt
#     import OpsinExperiment
#
#     filename = '../../experimental/abf_test/20160127_0016.abf'
#
#
#     f = ABFExperimentFile(filename)
#     xdata, ydata = f.import_data(block=0, segment=0, signal=0)
#     e = OpsinExperiment.OpsinExperiment(ydata)
#
#
#     e.set_data_density(50, mean_between_steps=True)
#
#     plt.plot(e.y)
#
#     plt.legend()
#     plt.show()