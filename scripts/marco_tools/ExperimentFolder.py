__author__ = 'marco'
import os


class ExperimentFolder:
    """
    represent and give access to an experimental folder
    """
    def __init__(self,folder,file_extension='txt'):
        self.folder = folder
        self.file_extension = file_extension
        self.selection = self.file_names_as_string_list

    def list_folder_generator(self):
        # get the experimental file names in self.folder
        if not (os.path.isdir(self.folder)) : raise Exception("{} is not a folder".format(self.folder))
        dirpath, dirnames, filenames = next(os.walk(self.folder))
        for el in filenames:
            yield el

    @property
    def selection_without_extensions(self):
        # ritorna una lista di stringhe della selezione di file, senza le estenzioni
        return [item.split('.')[0] for item in self.selection]

    def set_selection_without_extensions(self, name_list):
        # set the selection from a list of file names without extension
        self.selection = [item + '.' + self.file_extension for item in name_list]

    @property
    def file_names_as_int_list(self):
        filenumber = [];
        for el in self.list_folder_generator():
            try:
                value = int( el.split('.')[0] )
                filenumber.append( value )
            except Exception:
                pass
        return filenumber

    @property
    def file_names_as_string_list(self):
        fn = []
        for el in self.list_folder_generator():
            value = el.split('.')[1]
            if value == self.file_extension:
                fn.append(el)
        return fn

    # TODO : implement as gnerator, which is good in case of many many files

    # @property
    # def exp

if __name__ == "__main__":
    folder = ExperimentFolder("../../experimental/160225_CHO_ChR2_Marta", file_extension='abf')
    filename = folder.file_names_as_string_list
    print filename
