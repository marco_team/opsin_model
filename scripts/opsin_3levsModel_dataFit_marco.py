# -*- coding: utf-8 -*-
"""
Created on Fri Sep 25 11:15:06 2015

@author: marco
"""

from matplotlib.pyplot import *
from scipy import *
from scipy.integrate import odeint

from api.parseDatafile import *

ion()

n_data = 8

def setParametersForDataFit():
    """ these parameters are good enough for fitting Rossella's data"""
    global C,O,D,K_off,K_on,pulse_end,pulse_start,N,tau,v,alpha

    # popolazione totale
    N = 275
    pulse_start = 116
    pulse_end = pulse_end_times[n_data]
    
    # Is the transition light activated?
    C = 0   # Closed state
    O = 1   # Opens state
    D = 2   # Desensitized state
    
    # transition coefficients when light is OFF
    K_off = zeros([3,3])
    # transition coefficients when light is ON
    K_on =  zeros([3,3])

    tau = 25#17.9 # tau off [ms] Exp Physiol. 2011 Jan; 96(1): 19–25. 
    v = 1.0/tau
    alpha = 450# fit parameter
    beta = 1/0.00160771704180064# fit parameter
    
    K_off[O,C] = v
    K_on[O,C] = K_off[O,C]#*lightpower*beta
    
    K_off[C,O] = 0
    K_on[C,O] = v*lightpower*alpha
    
    K_off[O,D] = v/150*40
    K_on[O,D] = v/(150)*(lightpower*beta+40)
    K_off[D,O] = 0
    K_on[D,O] = v/30
#
#    K_off[O,D] = v/(3.2)
#    K_on[O,D] = K_off[O,D]
#        
#    K_off[D,O] = 0
#    K_on[D,O] = v/30*(lightpower*beta+40)

#    K_off[C,D] = 0        
#    K_on[C,D] = 0


#    K_off[D,C] = v/1000
#    K_on[D,C] = K_off[D,C]
    
def setParametersForModel(kon=False,koff=False):
    """ use this function for playing around with the parameters"""
    global C,O,D,K_off,K_on,pulse_end,pulse_start,N,tau,v,alpha

    # popolazione totale
    N = 250
    pulse_start = 116
#    pulse_end = pulse_end_times[n_data]
    pulse_end = 250
    
    # Is the transition light activated?
    C = 0   # Closed state0
    O = 1   # Opens state
    D = 2   # Desensitized state
    
    # transition coefficients when light is OFF
    K_off = zeros([3,3])
    # transition coefficients when light is ON
    K_on =  zeros([3,3])
    
    tau = 25#17.9 # tau off [ms] Exp Physiol. 2011 Jan; 96(1): 19–25. 
    v = 1.0/tau
    alpha = 191.4*2 # fit parameter
    
    K_off[O,C] = v
    K_on[O,C] = K_off[O,C]
    
    K_off[C,O] = 0
#    K_on[C,O] = v*lightpower*alpha
    K_on[C,O] = v*10
    
    K_off[O,D] = v/3
    K_on[O,D] = K_off[O,D]
    
    K_off[D,O] = v/0.7
    K_on[D,O] = K_off[D,O]

#    K_off[C,D] = v*0     
#    K_on[C,D] = K_off[C,D]*0

#    K_off[D,C] = v/10
#    K_on[D,C] = K_off[D,C]

def three_states_with_light(n,t):
    # just renameing for better convenience
    Nc = n[0]
    No = n[1]
    Nd = n[2]
    
    if t < pulse_start:
        K = zeros([3,3])
    elif t < pulse_end:
        K = K_on
    else:
        K = K_off

    
    return [
            -K[C,O]*Nc -K[C,D]*Nc +K[D,C]*Nd +K[O,C]*No, # = Nc'
            -K[O,D]*No -K[O,C]*No +K[C,O]*Nc +K[D,O]*Nd, # = No'
            -K[D,C]*Nd -K[D,O]*Nd +K[O,D]*No +K[C,D]*Nc # =Nd'
            ]

def experimentalParameters():
    global powervalues,pulse_end_times,dt
    dt = 0.1 #acquisition time [ms]
    #power
    powervalues = array([
    0.0836012861736334,
    0.0498392282958199,
    0.0249196141479099,
    0.0160771704180064,
    0.0128617363344051,
    0.00803858520900321,
    0.00643086816720257,
    0.00321543408360128,
    0.00160771704180064,
    ])
    powervalues = powervalues[::-1]
    
    pulse_end_times = array([
    225,215,205,195,185,175,165,155,135
    ])
    dt = 0.1    # ms
    
def plotData(n_data):
    global datalength
    experimentalParameters()
    # Experimental data
    folder = '/home/marco/Desktop/ospin_model/experimental/'
    filename = 'cell_100_ChR2.txt'

    data = parseDatafile(folder+filename,'\t')
    datalength = len(data[n_data])

    t = [it*dt for it in range(0,datalength)]
    plot(t,data[n_data],'.')
    
def plotModel():
    global pulse_end_times,lightpower
    func =  three_states_with_light
    
    # Kon = Koff*alpha*power20
    lightpower = powervalues[n_data]
    setParametersForDataFit()

    t = [it*dt for it in range(0,datalength)]
    
    yinit = [N,0,0]
    y = odeint(func,yinit,t,hmax=(pulse_end-pulse_start))
    
    #plot(t,y[:,0],t,y[:,1])
    #plot(t,y[:,0],'r',t,y[:,1])
    plot(t,-y[:,1],'r')
    plot(t,[-N*(K_on[C,O]/(K_on[C,O]+K_on[O,C]))]*len(t),'--')
    axis([100,300,-300,20])

def plateau(level):
    retval = 0
    a=N/3
    b=N/3
    c=N/3
    if level.upper() == 'C':
        retval = c+b+a
    elif level.upper() == 'O':
        retval = 0
    elif level.upper() == 'D':
        retval = 0
    return retval
        
def onlyModel():
    clf()
    global pulse_end_times,lightpower
    experimentalParameters()
    func =  three_states_with_light
    
    # Kon = Koff*alpha*power20
    lightpower = powervalues[n_data]
    setParametersForModel()

#    ligthpower = 1
#    alpha = 1

    t = [it*dt for it in range(0,datalength)]
    
    yinit = [N,0,0]
    y = odeint(func,yinit,t,hmax=(pulse_end-pulse_start))
    
    #plot(t,y[:,0],t,y[:,1])
    #plot(t,y[:,0],'r',t,y[:,1])
    plot(t,y[:,0],'r')
    plot(t,y[:,1],'b')
    plot(t,y[:,2],'g')
    
    
    # two levels model plateau
    kod = K_off[O,D]
    kdo = K_on[D,O]
    koc = K_off[O,C]
    kco = K_on[C,O]
    
    nor = (kco**2*kod**2)/(kdo**2*koc**2)+kco**2/koc**2+1
    const = 19#N/sqrt(nor)#19
    print const
    plot(t,[N*(K_on[C,O]/(K_on[C,O]+K_on[O,C]))]*len(t),'--')
    plot(t,[const]*len(t),'m-.')
    plot(t,[const*kco/koc]*len(t),'m-.')
    plot(t,[const*(kco*kod)/(kdo*koc)]*len(t),'m-.')
    print K_on[C,O]/K_off[O,C],(K_on[C,O]*K_off[O,D])/(K_off[O,C]*K_on[D,O])
    
    axis([100,300,-20,300])

def analyticalSolution(coeff,t):
    setParametersForModel()
    
    eigval = [[],[],[]]
    eigvec = [[],[],[]]
    y = [[],[],[]]
    kod = K_off[O,D]
    kdo = K_on[D,O]
    koc = K_off[O,C]
    kco = K_on[C,O]
    
    delta1 = kod**2+(2*koc+2*kdo-2*kco)*kod+koc**2+(2*kco-2*kdo)*koc+kdo**2-2*kco*kdo+kco**2
    delta2 = kod**2+2*koc*kod+2*kdo*kod-2*kco*kod+koc**2-2*kdo*koc+2*kco*koc+kdo**2-2*kco*kdo+kco**2
    eigval[0] = -(sqrt(delta1)+kod+koc+kdo+kco)/2
    eigval[1] = (sqrt(delta1)-kod-koc-kdo-kco)/2
    eigval[2] = 0
#    print "eigenvalues",eigval
    eigvec[0] = array([1,-(sqrt(delta2)+kod+koc+kdo-kco)/(2*koc),(sqrt(delta2)+kod-koc+kdo-kco)/(2*koc)])
    eigvec[1] = array([1,-(-sqrt(delta2)+kod+koc+kdo-kco)/(2*koc),(-sqrt(delta2)+kod-koc+kdo-kco)/(2*koc)])
    eigvec[2] = array([1,kco/koc,(kco*kod)/(kdo*koc)])
    print "eigenvectors",eigvec[0],eigvec[1],eigvec[2]
#    print  outer(coeff[0]*eigvec[0],exp(eigval[0]*0)) +\
#           outer(coeff[1]*eigvec[1],exp(eigval[1]*0)) +\
#           outer(coeff[2]*eigvec[2],exp(eigval[2]*0))
    return outer(coeff[0]*eigvec[0],exp(eigval[0]*t)) +\
           outer(coeff[1]*eigvec[1],exp(eigval[1]*t)) +\
           outer(coeff[2]*eigvec[2],exp(eigval[2]*t))

def plotAnalyticalSolution():
    figure(1)
    clf()
    experimentalParameters()
    datalength = 10000
    dt = 0.01
    t = array([it*dt for it in range(0,datalength)])
    coeff = array([225,5.5920,18.75])    
    y = analyticalSolution(coeff,t)
    plot(t,y[0],'g')
    plot(t,y[1],'b')
    plot(t,y[2],'r')


#plotData(n_data)
#plotAnalyticalSolution()
#show()
#axis('auto')

n_data=0
figure(1)
clf()
plotData(n_data)    
plotModel()
figure(2)
clf()
n_data=8
plotData(n_data)    
plotModel()
    
#onlyModel()
#show()
