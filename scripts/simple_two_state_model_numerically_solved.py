# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 17:02:28 2015

@author: marco
"""
from matplotlib.pyplot import *
from scipy import *
from scipy.integrate import odeint

# popolazione totale
N = 1


def func(n,t):
    # costante di promozione dal livello zero al livello 1
    k01 = 1/0.2
    # costante di decadimento dal livello 1 al livello 0
    k10 = k01/2
    return [k10*n[1]-k01*n[0],-k10*n[1]+k01*n[0]]
    
    
# ascisse
t = linspace(0,1,100)
yinit = [N,0]
y = odeint(func,yinit,t)

figure(1)
plot(t,y[:,0],t,y[:,1])
show()