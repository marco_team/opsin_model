# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 16:14:50 2015

@author: marco
"""
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

# soluzione analitica di un problema a due livelli
# livello 1 inizialmente vuoto

# costante di promozione dal livello zero al livello 1
k01 = 1/0.2
# costante di decadimento dal livello 1 al livello 0
k10 = k01/2

# popolazione totale
N = 1

# ascisse
t = np.arange(0,1,0.01)

# soluzione analitica
k = k01 + k10
n1 = N/k*k01*(1-np.exp(-k*t))
n0 = N/k*(k10+k01*np.exp(-k*t))

plt.figure(1)
plt.plot(t,n1,'b',t,n0,'r')
plt.ylim(0,1) 
#plt.plot(n0,n1,'r')
#plt.ylim(-N,N) 
#plt.xlim(-N,N) 
plt.show()