# -*- coding: utf-8 -*-
"""
Created on Fri Sep 25 17:46:02 2015

@author: marco
"""

from api.parseDatafile import *

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    folder = '/home/marco/Desktop/ospin_model/experimental/'
    filename = 'cell_100_ChR2.txt'
    with open(folder+'test.txt','wb') as temptestfile:
        temptestfile.write('dataTypeA\tdataTypeB\n')
        for it in range(10):
            temptestfile.write('dataTypeA\tdataTypeB\n')
        
#    filename = 'test_data.txt'
    (names,data) = parseDatafile(folder + filename,'\t')
    x_val = range(0,len(data[names[0]]))
    
    for n in names:
        plt.plot(x_val,data[n],'.')
        
    plt.show()