from matplotlib.pyplot import plot, axis, subplot, title, figure

from numpy.linalg import norm
from numpy import array, max, zeros, linspace, copy
from scipy.integrate import odeint

import sys
sys.path.append('../')

from opsine_model_utils.utils import Pulse
from opsine_model_utils.analytical_solver import PulsedSolver
from opsine_model_utils.models import ode_model_generic
from api.parseDatafile import *



# %%

# Experimental data
folder = '../experimental'
filename = '/cell_100_ChR2.txt'

data = array(parseDatafile( folder + filename,'\t' ))
#datalength = len(data[n_data])
#t = [it*dt for it in range(0,datalength)]

for i,d in enumerate(data):
    d = d/min(data[-1])
    plot(d[1000:-2000:10],'-');
    data[i] = d
#axis([90,300,0,1.2])
    
    
    
    
    
dt = 0.1 #acquisition time [ms]

#power (I don't know in what unit)
powervalues = array([
0.0836012861736334,
0.0498392282958199,
0.0249196141479099,
0.0160771704180064,
0.0128617363344051,
0.00803858520900321,
0.00643086816720257,
0.00321543408360128,
0.00160771704180064,
])

powervalues = powervalues[::-1] # invert the order

# the pulse end time varies for each experiment
pulse_end_times = array( [225,215,205,195,185,175,165,155,135] )


#%%


# CONSTANTS DEFINING THE STATES
C = 0   # Closed state
O = 1   # Opens state
D = 2   # Desensitized state

# TRANSITION RATES
K_off = zeros([3,3]) # transition coefficient MATRIX when light is OFF
K_on =  zeros([3,3]) # transition coefficient MATRIX when light is ON

tau = 25 #17.9 # usually called "tauoff" [ms]. 
k = 1.0/tau

alpha = 450# fit parameter
beta = 1/0.00160771704180064# fit parameter

K_off[O,C] = k
K_on[O,C] = K_off[O,C] # this transition does not depend on light

K_off[C,O] = 0
K_on[C,O] = k*lightpower*alpha

K_off[O,D] = k/150*40
K_on[O,D] = k/(150)*(lightpower*beta+40)

K_off[D,O] = k/30
K_on[D,O] = K_off[D,O] # k/30



n_data = 7
lightpower = powervalues[n_data]       # set the light dependent parameters
K_on[C,O] = k*lightpower*alpha
K_on[O,D] = k/(150)*(lightpower*beta+40)

pulse_start = 115
pulse_end = pulse_end_times[n_data]
pulse_duration = pulse_end-pulse_start
pulse = Pulse(115,pulse_duration,500)

t = [it*dt for it in range(0,len(data[n_data]))]
v = odeint(ode_model_generic,[1,0,0],t,(K_off,K_on,pulse))
s = PulsedSolver(K_on,K_off,[1,0,0],t,pulse)

figure()
plot(t,data[n_data],'b');
plot(t,v[:,1],'r--',linewidth='2');
plot(t,s.solution()[1],'g--',linewidth='2');

show()