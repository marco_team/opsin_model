# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 14:47:08 2015
Simple script for importing and plotting data from a file
@author: marco
"""

import csv


def parseDatafile(datafile, delimiter, quotechar='#'):
    with open(datafile,'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiter, quotechar=quotechar)
        waste = reader.next()
        digested = [[] for i in range(0,len(waste))]
        for rn,row in enumerate(reader):
            for it,elm in enumerate(row):
                digested[it].append(float(elm))
#            if rn == 2: break
#        print digested
    return digested

def parseDatafile_in_dict(datafile, delimiter, quotechar='#'):
    digested = {}
    with open(datafile,'rb') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=delimiter, quotechar=quotechar)
        digested = digested.fromkeys(reader.next(),[])
        for row in reader:
            for label in row.keys():
                digested[label] = digested[label]+[row[label]]
                
    return (digested.keys(),digested)

# test
if __name__ == "__main__":
    import matplotlib.pyplot as plt
    folder = '/home/marco/Desktop/ospin_model/experimental/'
    filename = 'cell_100_ChR2.txt'
#    with open(folder+'test.txt','wb') as temptestfile:
#        temptestfile.write('dataTypeA\tdataTypeB\n')
#        for it in range(10):
#            temptestfile.write('dataTypeA\tdataTypeB\n')
        
#    filename = 'test_data.txt'
    data = parseDatafile(folder + filename,'\t')
#    print data
    x_val = range(0,len(data[1]))
#    x_val = range(0,len(data[names[0]]))
    
    plt.plot(x_val,data[0],'.')
    

    
    plt.show()