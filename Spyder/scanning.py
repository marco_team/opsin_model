# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 14:11:02 2016

@author: marco
"""

import sys
# include the toolkit path in the modules paths
sys.path.append('../scripts')

# graphics
import matplotlib.pyplot as plt
#import seaborn as sns

# maths
import numpy as np
from scipy.integrate import odeint
from scipy.optimize import curve_fit

## the opsin-model toolkit
import marco_tools as mt

import quantities as pq

# plotting constants
FIG_BIG = (15,10)
FIG_H = (15,7)
FIG_SMALL = (7,7)

# help setting the text in a plot
def default_plot_setup(title='Title', xlabel='x',ylabel='y'):
    plt.title(title);
    plt.xlabel(xlabel)
    plt.ylabel(ylabel);


# ANIMATION
import pylab
from matplotlib import animation

import pickle

#%% MODEL CONSTANTS

# CONSTANTS DEFINING THE STATES
C = 0   # Closed state
O = 1   # Opens state
D = 2   # Desensitized state

# TRANSITION RATES MATRICES
K_off = np.zeros([3,3]) # transition coefficient MATRIX when light is OFF
K_on =  np.zeros([3,3]) # transition coefficient MATRIX when light is ON

K_on = pq.Quantity(K_on,pq.ms**-1)
K_off = pq.Quantity(K_off,pq.ms**-1)

tau_off = pq.Quantity(21,'ms')

tau = tau_off #17.9 # usually called "tauoff" [ms] Exp Physiol. 2011 Jan; 96(1): 19–25. 
k = 1.0/tau

K_off[O,C] = 1.0/tau_off
K_on[O,C] = 1.0/tau_off

tau_on = pq.Quantity(30,'ms')
K_on[C,O] = 1.0/tau_on

#K_off[C,O] = 0
#K_on[C,O] = k

#K_off[O,D] = 0
K_on[O,D] = pq.Quantity(0.03, pq.ms**-1)

#K_off[D,O] = k/30
#K_on[D,O] = K_off[D,O] # k/30

#%% MODELS

def ode_model_generic_on_off(y, t, k):
    dy = np.empty_like(y)
#    dy = pq.Quantity(dy, pq.ms**-1)

    # ODE system
    for i, f in enumerate(y):
        dy[i] = -f * sum(k[i, :]) + np.dot(y, k[:, i])  # short notation for the linear system

    return dy

# Basic square integration
def integr(y,dt,k):
    dy = np.empty_like(y) # * pq.Quantity(1,pq.ms**-1)
#    dy = pq.Quantity(dy, pq.ms**-1)
    
    for i, f in enumerate(y):
        dy[i] = (-f * sum(k[i, :]) + np.dot(y, k[:, i])) # *pq.Quantity(1,pq.ms**-1)
    
    return y + dy*dt

#%% SCANNING FUNCTIONS

tau_on_dimless = tau_on.magnitude

def uniform_illumination(t,x,y):
    if t > simul["pulse duration"]:
        k = 0
    else:        
        k = 1.0/tau_on_dimless
                 
    return k

def circle_scanner(t,x,y):
    if t > simul["pulse duration"]:
        k = 0
    else:
        circle = (0.4*np.cos(t*omega_dl), 0.4*np.sin(t*omega_dl))
        f = np.exp( - ((lin[x]-circle[0])**2+ (lin[y]-circle[1])**2) / (2*(0.03)**2) )
        k = 1.0/tau_on_dimless * f
                 
    return k

def raster_scanner(t,x,y):
    direction = 1
    hsteps = 111
    vsteps = 10
    if t > simul["pulse duration"]:
        k = 0
    else:
        j = int(t/T*vsteps*hsteps)
        if (j//hsteps)%2 == 0:
            direction *= -1
        
        center = (direction*((j%hsteps)*1.0/hsteps-0.5), (j//hsteps)*1.0/vsteps-0.5)
        
#        center = ((j%10-5)*1.0/9, (10-(j//10-5))*1.0/9)
        f = np.exp( - ((lin[x]-center[0])**2+ (lin[y]-center[1])**2) / (2*(0.03)**2) )
        k = 2*1.0/tau_on_dimless * f
                 
    return k

def spiral_scanner(t,x,y):
    if t > simul["pulse duration"]:
        k = 0
    else:
        omega = 2*np.pi/100
        th = t*omega
        r = t/1000/2
        center = (r*np.cos(th),r*np.sin(th))
        
#        center = ((j%10-5)*1.0/9, (10-(j//10-5))*1.0/9)
        f = np.exp( - ((lin[x]-center[0])**2+ (lin[y]-center[1])**2) / (2*(0.03)**2) )
        k = 2*1.0/tau_on_dimless * f
                 
    return k


def spiral_cspeed(t,x,y):
    if t > simul["pulse duration"]:
        k = 0
    else:
        v = 1.0/1000/2 # 1 / simul time / 2
        omega = 2*np.pi/100
        th = t*omega
#        r = np.sqrt(v/2/t) * t
        r = np.sqrt(v/(1+(t**2)*(omega**2))) * t # NOT WORKING
        
        center = (r*np.cos(th),r*np.sin(th))
        
#        center = ((j%10-5)*1.0/9, (10-(j//10-5))*1.0/9)
        f = np.exp( - ((lin[x]-center[0])**2+ (lin[y]-center[1])**2) / (2*(0.03)**2) )
        k = 2*1.0/tau_on_dimless * f
                 
    return k


#img = np.zeros((N,N))
#for y in range(N):
#    for x in range(N):
#        img[y,x] = scanning_function(500,x,y)
#
#plt.imshow(img, cmap='hot',
#           interpolation='none')
#plt.colorbar()
#%%
simul_raster = {
        "start time" : pq.Quantity(0.0, 'ms'),
        "end time" : pq.Quantity(10000.0, 'ms'),
        "time points" : 1000,
        "pulse duration" : pq.Quantity(10000.0, 'ms'),
        "surface area"   : pq.Quantity(300.0, pq.um**2),
        "surface side points": 20,
        "cell max current" : pq.Quantity(300.0, pq.pA),
        "cell membrane thinckness" : pq.Quantity(7, pq.nm),
        "resting potential" : pq.Quantity(-70.0, pq.mV),
        "light_power" : pq.Quantity(0.08, pq.W * pq.m**-2),
        "spike potential threshold" : pq.Quantity(-50.0, pq.mV),
        "scanning function" : raster_scanner
    }


simul_spiral = {
        "start time" : pq.Quantity(0.0, 'ms'),
        "end time" : pq.Quantity(1000.0, 'ms'),
        "time points" : 1000,
        "pulse duration" : pq.Quantity(1000.0, 'ms'),
        "surface area"   : pq.Quantity(300.0, pq.um**2),
        "surface side points": 20,
        "cell max current" : pq.Quantity(300.0, pq.pA),
        "cell membrane thinckness" : pq.Quantity(7, pq.nm),
        "resting potential" : pq.Quantity(-70.0, pq.mV),
        "light_power" : pq.Quantity(0.08, pq.W * pq.m**-2),
        "spike potential threshold" : pq.Quantity(-50.0, pq.mV),
        "scanning function" : spiral_scanner
    }


simul_spiral_cspeed = {
        "start time" : pq.Quantity(0.0, 'ms'),
        "end time" : pq.Quantity(1000.0, 'ms'),
        "time points" : 200,
        "pulse duration" : pq.Quantity(1000.0, 'ms'),
        "surface area"   : pq.Quantity(300.0, pq.um**2),
        "surface side points": 20,
        "cell max current" : pq.Quantity(300.0, pq.pA),
        "cell membrane thinckness" : pq.Quantity(7, pq.nm),
        "resting potential" : pq.Quantity(-70.0, pq.mV),
        "light_power" : pq.Quantity(0.08, pq.W * pq.m**-2),
        "spike potential threshold" : pq.Quantity(-50.0, pq.mV),
        "scanning function" : spiral_cspeed
    }


simul_uniform = {
        "start time" : pq.Quantity(0.0, 'ms'),
        "end time" : pq.Quantity(1000.0, 'ms'),
        "time points" : 1000,
        "pulse duration" : pq.Quantity(800.0, 'ms'),
        "surface area"   : pq.Quantity(300.0, pq.um**2),
        "surface side points": 5,
        "cell max current" : pq.Quantity(300.0, pq.pA),
        "cell membrane thinckness" : pq.Quantity(7, pq.nm),
        "resting potential" : pq.Quantity(-70.0, pq.mV),
        "light_power" : pq.Quantity(0.08, pq.W * pq.m**-2),
        "spike potential threshold" : pq.Quantity(-50.0, pq.mV),
        "scanning function" : uniform_illumination
    }

simul = simul_uniform

scanning_function = simul["scanning function"]

side = simul["surface area"]**0.5
light_power = simul["light_power"].base

timespace = np.linspace( simul["start time"], simul["end time"], simul["time points"] )
timespace_d = pq.Quantity(timespace,simul["start time"].units)
dt = (timespace[-1]-timespace[0])/(len(timespace)-1)
dt_d = pq.Quantity(dt,simul["start time"].units)

# max current per pixel is max current / number of pixels
max_current_density = simul["cell max current"]/simul["surface area"]
pixel_surface = simul["surface area"]/(simul["surface side points"]**2)
max_current_per_pixel = max_current_density * pixel_surface

print dt,dt_d
#%%
# SOLVE THE DIFFERENTIAL EQUATION SYSTEM with the given parameters
#fig = plt.figure(1)
#
#initial_condition = [1,0,0]
#
## step integration
#
#k = K_on
#
#v = np.zeros((len(timespace) , 3))
#v[0] = initial_condition
#
#int_step = 2
#for i in range(1,len(timespace)):
#    
#    if i == simul["points"]/2:
#        k = K_off
#    v[i] = odeint(ode_model_generic_on_off, 
#               v[i-1],
#               np.linspace(0,dt,int_step),
#               tuple([k]),
#               hmax=dt)[-1]
#
#plt.plot(timespace, v[:,1]);
#plt.show()
# plt.axis((0,1000,0,1.2));


   
#%%

N = simul["surface side points"]
lin = np.linspace(-0.5,0.5,N)
meshX, meshY = np.meshgrid(lin,lin)
R2 = meshX**2 + meshY**2
R = np.sqrt(R2)


T = simul["end time"] - simul["start time"]
omega = (2*np.pi)/T


T_dl = T.base
omega_dl = omega.base

#%%

A = np.zeros((simul["time points"], 3, N, N))
# setting initial condition A[time,population,X,Y]
A[0,0,:,:] = np.ones((N,N))
#A = pq.Quantity(A, pq.dimensionless)

#%%


k = K_on.base

print "0% ||||||||||||||||||||||||||||||||||||||||||| 100%"
print "   [",
    
    
temp_index_max = len(timespace)-1
asterisco = 0

for i in range(1,temp_index_max):  
    t = timespace[i]
    sonda = int(i*1.0/temp_index_max*100)
    if sonda%5 == 0 and sonda/5>asterisco :
        print '*' , #"running : {}%".format(np.round(t/T.base*100))
        asterisco += 1
    for j in range(N**2): #scan the 2d matrix
        y, x = j%N , j/N        
        k[C,O] = scanning_function(t,x,y)
        A[i,:, y, x] = integr(A[i-1,:, y , x],dt,k)

print "* ]"

A = A[:,1,:,:]
A_d = A * max_current_per_pixel
A = A_d.magnitude

#%%


def current_plot(t,i,*args):
    ax = plt.plot(t,i,*args)
    plt.xlabel(t.dimensionality.latex)
    plt.ylabel(i.dimensionality.latex)
    return ax

def surfaceplot(s,*args):
    global norm
    im1 = plt.imshow(s, cmap='hot', norm = norm, interpolation='none')
    im1.set_extent([0,side.base,0,side.base])
    plt.xlabel(side.dimensionality.latex)
    plt.ylabel(side.dimensionality.latex)
    return im1

#print m

#fig = plt.figure(2)
#plt.subplot(2,2,1)
#surfaceplot(A[0,:,:])
#plt.subplot(2,2,2)
#surfaceplot(A[2,:,:])
#plt.subplot(2,2,3)
#surfaceplot(A[-1,:,:])
#plt.subplot(2,2,4)
#current_plot(timespace_d, A_d[:,N/2,N/2])


#%%

fig = plt.figure(3)
ax1 = fig.add_subplot(1,1,1)

# im1 = ax1.imshow(s0[:,:,0], interpolation = 'none',norm=norm,
#                  extent = [-Xmax, Xmax, -Xmax, Xmax])

A = A_d.magnitude

m = np.max(A)
norm = pylab.mpl.colors.Normalize(vmin=0, vmax=m)

plt.subplot(2,2,1)
plt.tight_layout()
im1 = surfaceplot(A[0,:,:])

def init():
    im1.set_data(A[0,:,:])
    return([im1])

                                              
def animate(t):
    im1.set_data(A[t,:,:])
    return([im1])

anim = animation.FuncAnimation(fig,
                               animate,
                               simul["time points"],
                               interval=10,
                               init_func=init,
                               repeat=True)



rest_v = simul["resting potential"]
m_thick = simul["cell membrane thinckness"]
spike_threshold = simul["spike potential threshold"]

cell_radius = pq.Quantity(15, pq.um)
cell_surface = 4 * np.pi * cell_radius**2

cell_capacitance_density = pq.Quantity(10**-2, pq.F*pq.m**-2)
cell_capacitance = cell_capacitance_density * cell_surface

I = np.sum(A_d,axis=(1,2))
q = np.cumsum(I*dt_d).rescale(pq.C)

# ELECTRIC FIELD
#epsilon0 = pq.electric_constant # *8*10**-12
#ke = 1.0/(np.pi*4*(epsilon0))
#E =  ke * q / (m_thick**2)

# ELECTRIC POTIENTIAL
#dv = rest_v + np.cumsum(E * m_thick)
# spherical capacirot
#dv = rest_v + np.cumsum(q*ke * abs(1/cell_radius - 1/(cell_radius+m_thick)))

dv = rest_v + q/cell_capacitance
dv = dv.rescale('mV')

#spike_time = timespace(np.find(abs()))
spike_index = abs(dv - spike_threshold).argmin()
spike_time = timespace_d.magnitude[spike_index]
print "Spike time :", spike_time

plt.subplot(2,2,2)
current_plot(timespace_d,I,'-')
plt.title("total current")
plt.tight_layout()

plt.subplot(2,2,3)
current_plot(timespace_d,q,'-')
plt.title("total charge")
plt.tight_layout()

plt.subplot(2,2,4)
current_plot(timespace_d[:spike_index+1],dv[:spike_index+1],'-')
plt.plot((spike_time,spike_time),(-70,np.max(dv.magnitude)),'k--')
#current_plot(timespace_d[spike_index+1:],dv[spike_index+1:],'--')
plt.title("membrane potential")
plt.tight_layout()


#%%

def save_current_stauts(filename):
    global A_d, timespace_d, simul, dt_d
    topickle = {
        "timespace" : timespace_d,
        "current matrix" : A_d,
        "simulation patameters": simul,
        "dt_d" : dt_d
    }
    
    with open(filename,'wb') as f:
        pickle.dump(topickle, f)
    
def load_status(filename):
    global A_d, timespace_d, simul, dt_d
    
    with open (filename,'rb') as f:
        tounpickle = pickle.load(f)    
        timespace_d = tounpickle["timespace"]
        A_d = tounpickle["current matrix"]
        simul = tounpickle["simulation patameters"]
        dt_d = tounpickle["dt_d"]
    
    
#%%
    
#save_current_stauts("uniform.pkl")
#save_current_stauts("raster.pkl")
#save_current_stauts("spiral.pkl")
#save_current_stauts("spiral_cspeed.pkl")
#save_current_stauts("raster_slow_10s.pkl")

#load_status("./uniform.pkl")
#load_status("./raster.pkl")
#load_status("./spiral.pkl")
#load_status("./spiral_cspeed.pkl")

#%%


#def test(t):
#        omega = 2*np.pi/100
#        th = t*omega
#        r = t/1000/2
#        center = (r*np.cos(th),r*np.sin(th))
#        
#        return r*np.cos(th), r*np.sin(th)
#
#
#def test_cs(t):
#        r = t/1000/2        
#        omega = 2*np.pi/r
#        th = omega*t
#        
#        
#        return r*np.cos(th), r*np.sin(th)
#        
##print  test(timespace)
#
#tt = np.linspace(0,1000,1000)
#
#plt.subplot(1,2,1)
#plt.plot(*test(tt))
#plt.axis('equal')
#
#plt.subplot(1,2,2)
#plt.plot(*test_cs(tt))
#plt.axis('equal')