# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 17:10:46 2016

@author: marco
"""


import sys
# include the toolkit path in the modules paths
sys.path.append('../scripts')

# graphics
import matplotlib.pyplot as plt
import seaborn as sns

# maths
import numpy as np
from scipy.integrate import odeint
from scipy.optimize import curve_fit

## the opsin-model toolkit
import marco_tools as mt

# plotting constants
FIG_BIG = (15,10)
FIG_H = (15,7)
FIG_SMALL = (7,7)

# help setting the text in a plot
def default_plot_setup(title='Title', xlabel='x',ylabel='y'):
    plt.title(title);
    plt.xlabel(xlabel)
    plt.ylabel(ylabel);


# ANIMATION
import pylab
from matplotlib import animation

#%%

# CONSTANTS DEFINING THE STATES
C = 0   # Closed state
O = 1   # Opens state
D = 2   # Desensitized state

# TRANSITION RATES MATRICES
K_off = np.zeros([3,3]) # transition coefficient MATRIX when light is OFF
K_on =  np.zeros([3,3]) # transition coefficient MATRIX when light is ON


tau_off = 18

tau = tau_off #17.9 # usually called "tauoff" [ms] Exp Physiol. 2011 Jan; 96(1): 19–25. 
k = 1.0/tau

K_off[O,C] = k
K_on[O,C] = k

tau_on = 50
K_on[C,O] = 1.0/tau_on

#%%

simul = {
        "start time" : 0,
        "end time" : 1000,
        "points" : 1000,
        "pulse duration" : 900,
        "side" : 50
    }

timespace = np.linspace( simul["start time"], simul["end time"], simul["points"] )
#dt = (timespace[-1]-timespace[0])/(len(timespace)-1)
dt = tau/10

N = simul["side"]
lin = np.linspace(-0.5,0.5,N)
meshX, meshY = np.meshgrid(lin,lin)
R2 = meshX**2 + meshY**2
R = np.sqrt(R2)
T = simul["end time"] -  simul["start time"]
omega = (2*np.pi)/simul["end time"]


#def raster_scanner(t,x,y):
#    if t > simul["pulse duration"]:
#        k = K_off
#    else:
#        j = int(t/(simul["end time"]-simul["start time"])*100) # j goes from 0 to 100
#        center = ((j%10-5)*1.0/10, (j//10-5)*1.0/10)
##        center = ((j%10-5)*1.0/9, (10-(j//10-5))*1.0/9)
#        f = np.exp( - ((lin[x]-center[0])**2+ (lin[y]-center[1])**2) / (2*(0.03)**2) )
##        K_on[C,O] = 1.0/tau_on * f
##        f = np.exp( - ((lin[x]-lin[0])**2+ (lin[y]-lin[1])**2) / (2*(0.03)**2) )
#        K_on[C,O] = 1.0/tau_on * f
#        k = K_on
#                 
#    return k


def raster_scanner(t,x,y):
    direction = 1
    hsteps = 100
    vsteps = 10
    if t > simul["pulse duration"]:
        k = K_off
    else:
        j = int(t/T*vsteps*hsteps)
        if (j//hsteps)%2 == 0:
            direction *= -1
        
        center = (direction*((j%hsteps)*1.0/hsteps-0.5), (j//hsteps)*1.0/vsteps-0.5)
        
#        center = ((j%10-5)*1.0/9, (10-(j//10-5))*1.0/9)
        f = np.exp( - ((lin[x]-center[0])**2+ (lin[y]-center[1])**2) / (2*(0.03)**2) )
        K_on[C,O] = 1.0/tau_on * f        
        k = K_on
                 
    return k

def circle_scanner(t,x,y):
    if t > simul["pulse duration"]:
        k = K_off
    else:
#        r = 0.5*(t/simul["end time"])
        circle = (0.4*np.cos(t*omega), 0.4*np.sin(t*omega))
        f = np.exp( - ((lin[x]-circle[0])**2+ (lin[y]-circle[1])**2) / (2*(0.03)**2) )
#        K_on[C,O] = 1.0/tau_on * f
#        f = np.exp( - ((lin[x]-lin[0])**2+ (lin[y]-lin[1])**2) / (2*(0.03)**2) )
        K_on[C,O] = 1.0/tau_on * f
        k = K_on
                 
    return k

scanning_function = raster_scanner

def ode_model_generic_on_off(y, t, k, xx, yy):
    
    dy = np.empty_like(y)
    
    
    k = scanning_function(t, xx, yy)

    # ODE system
    for i, f in enumerate(y):
        dy[i] = -f * sum(k[i, :]) + np.dot(y, k[:, i])  # short notation for the linear system

#    dy[C] = -y[C]*k[C,O] + y[O]*k[O,C] + y[D]*k[D,C]
#    dy[O] = -y[O]*(k[O,C]+k[O,D]) + y[C]*k[C,O]
#    dy[D] = -y[D]*k[D,C] + y[O]*k[O,D]


    return dy

#%%
# SOLVE THE DIFFERENTIAL EQUATION SYSTEM with the given parameters

#fig = plt.figure(1)
#
#initial_condition = [1,0,0]
#
## step integration
#
#k = K_on
#
#v = np.zeros_like(timespace)
#v = odeint(ode_model_generic_on_off, 
#              initial_condition,
#              timespace,
#              (k,0,0),
#              hmax=dt)[:,1]
#plt.plot(timespace, v);
#plt.show()
## plt.axis((0,1000,0,1.2));


#%%

A = np.zeros((simul["points"], N, N))
#setting initial condition A[time,population,X,Y]
#A[0,0,:,:] = np.ones((N,N))

k = K_on
initial_condition = [1,0,0]
v = np.zeros_like(timespace)


                 
#im = plt.imshow(A[0,1,:,:], cmap=plt.get_cmap('jet'))
#plt.show()


#f = open('temp.txt','w')

for y in range(N): #scan the 2d matrix
    print "running : {}%".format(np.round(y * 1.0/N*100))
    B = np.zeros((simul["points"],N))
    for x in range(N):    
        A[:, y , x] = odeint(ode_model_generic_on_off, 
            initial_condition,
            timespace,
            (k,x,y),
            hmax=tau_on)[:,1] #,


#f.close()
#plt.plot(A[:,1,1,1])
#%%

fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)

# im1 = ax1.imshow(s0[:,:,0], interpolation = 'none',norm=norm,
#                  extent = [-Xmax, Xmax, -Xmax, Xmax])

    
m = np.max(A[:])
norm = pylab.mpl.colors.Normalize(vmin=0, vmax=m)

im1 = ax1.imshow(A[0,:,:], cmap='hot',
                 interpolation='none', norm = norm)


def init():
    im1.set_data(A[0,:,:])
    return([im1])

                                              
def animate(t):
    im1.set_data(A[t,:,:])
    return([im1])


anim = animation.FuncAnimation(fig,
                               animate,
                               simul["points"],
                               interval=1,
                               init_func=init,
                               repeat=True)
                               
#%%
#x = np.arange(simul["points"])
#
#def f(t):
#    return np.sum(A[0:t,:,:])
#
#plt.plot(x,[f(i) for i in x],'.')