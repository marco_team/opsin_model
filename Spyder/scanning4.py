# -*- coding: utf-8 -*-
"""
Created on Wed Apr  6 13:25:02 2016

@author: marco
"""


# IMPORTS 

import sys
# include the toolkit path in the modules paths
if '../scripts' not in sys.path:
    sys.path.append('../scripts')

## the opsin-model toolkit
import marco_tools as mt


# graphics
import matplotlib.pyplot as plt

# maths
import numpy as np
from scipy.integrate import odeint
from scipy.optimize import curve_fit

import quantities as pq

# plotting constants
FIG_BIG = (15,10)
FIG_H = (15,7)
FIG_SMALL = (7,7)

# help setting the text in a plot
def default_plot_setup(title='Title', xlabel='x',ylabel='y'):
    plt.title(title);
    plt.xlabel(xlabel)
    plt.ylabel(ylabel);

import itertools

# ANIMATION
import pylab
from matplotlib import animation

import pickle


from multiprocessing import Process

#%% tau_on vs lightpower relation

# fitted parameters
# this parameters are obtained by fitting Mara's experiment
# the data are in file : '2016_02_25c_0005.abf'
#
powervalues = [0.00156757, 0.00333333, 0.00522523,
               0.00720721, 0.00846847, 0.01108108,  0.01306306]

opsin_dynamics_parameters = [ 0.10227079 ]

kod_mean = pq.Quantity(0.011580810715, pq.ms**-1)
kdc_mean = pq.Quantity(0.00464194058573, pq.ms**-1)

tau_off_dl = 9.33764144685

#def func_on(x,m,q,c,a):
#    temp_tau_on = m / (a*x + q) + c
#    if (a*x + q) > 0:
#         temp_tau_on =  10**4
#    return float(temp_tau_on)
    
def func_on(x,m):
    return m/x
    
#%% INTEGRATION FUNCTION

def integr(y,dt,k):
    dy = np.empty_like(y) # * pq.Quantity(1,pq.ms**-1)
    # dy = pq.Quantity(dy, pq.ms**-1)
    for i, f in enumerate(y):
        dy[i] = (-f * sum(k[i, :]) + np.dot(y, k[:, i])) # *pq.Quantity(1,pq.ms**-1)
    
    return y + dy*dt


#%% SCANNING FUNCTIONS


def uniform_illumination(t,x,y,pulse_duration,tau_on,**argv):
    if t > pulse_duration:
        k = 0
    else:
        k = 1.0/tau_on
    return k

#%% TEST RASTER SCANNER

scan_point_list = []
#I_scan = None


def raster_scan(t,x,y,pulse_duration,tau_on):    
    """ generates pixel coordinates for taster scanning,
    corresoinding to instant t """
    if t > pulse_duration:
        k = 0
    else:
        ti = (t/T).magnitude
        ni = int(ti*len(raster_scan_trajectory))
        center_x = raster_scan_trajectory[ni][1]
        center_y = raster_scan_trajectory[ni][0]

#        # THE FOLLOWIG LINES DO THE CALCULATION FOR K AT EACH CALL        
#        sigma = simul[ "psf_width"].magnitude
#        f = 1.0/np.sqrt(2)/sigma * \
#            np.exp( - ((s1[x]-s1[center_x])**2 + (s1[y]-s1[center_y])**2) / (2*(sigma)**2) )
#        
#        tau_on_temp = func_on(f*lightpower,*opsin_dynamics_parameters)
#        k = 1.0/tau_on_temp
#        
        
        # THIS LINE USES THE PRE BUILT tau_on MAP
        k = 1.0/tau_on_map[N+(center_x-x),N+(center_y-y)]

    return k
    

def raster_scan_test():
    temp = np.zeros((N,N))  
    for j in range(N2): #scan the 2d matrix
        y, x = j%N , j/N
        temp[y,x] = raster_scan(1.2,x,y,15,tau_on_dl)
        
    plt.imshow(temp, interpolation='None')

#raster_scan_test()
#%%

processes_num = 2

# the field of view is a square matrix
simul_uniform = {
    "time steps" : 50,
    "pixels per side": 30,
    
    "simulation time" : pq.Quantity(600.0, 'ms'),
    "pulse duration" : pq.Quantity(500.0, 'ms'),

    "field of view side" : pq.Quantity(15.0, pq.um),
    "cell radius" : pq.Quantity(5, pq.um),

    "cell max current" : pq.Quantity(141.429035808, pq.pA),
    "resting potential" : pq.Quantity(-70.0, pq.mV),
    "light power" : pq.Quantity(powervalues[4], pq.W * pq.um**-2),
    "spike potential threshold" : pq.Quantity(-50.0, pq.mV),
    "scanning function" : uniform_illumination, #mt.uniform_illumination,
    "scanning functions args" : {},
    
    "integration" : "odeint"
#    "integration" : "normal"
    }


simul_raster = {
    "time steps" : 10**2,
    "pixels per side": 61,
    
    "simulation time" : pq.Quantity(15.0, 'ms'),
    "pulse duration" : pq.Quantity(15.0, 'ms'),

    "field of view side" : pq.Quantity(15.0, pq.um),
    "cell radius" : pq.Quantity(5, pq.um),

    "cell max current" : pq.Quantity(141.429035808, pq.pA),
    "resting potential" : pq.Quantity(-70.0, pq.mV),
    "light power" : pq.Quantity(powervalues[4]*5, pq.W * pq.um**-2),
    "spike potential threshold" : pq.Quantity(-50.0, pq.mV),
    "scanning function" : raster_scan, #mt.uniform_illumination,
    "psf_width" : pq.Quantity(1, pq.um),
    "raster_dy" : pq.Quantity(0.5, pq.um),
    
    "integration" : "odeint"
#    "integration" : "normal"
    }

simul = simul_uniform

scanning_function = simul["scanning function"]

   

# Variable names ending with _dl are dimention-less

# simulation time
T = simul["simulation time"]
N = simul["pixels per side"]
N2 = N**2
l = simul["field of view side"]

T_dl = T.magnitude
l_dl = l.magnitude


# time space for this simulation
timespace_dl = np.linspace(0, simul["simulation time"].magnitude,
                           simul["time steps"])
timespace = pq.Quantity(timespace_dl,simul["simulation time"].units)

# time element
dt = (timespace[-1]-timespace[0])/(len(timespace)-1)
dt_dl = dt.magnitude

lightpower = simul["light power"].magnitude
pulse_duration = simul["pulse duration"]
# max current per pixel
# This is calculated by dividing the estimed max current (fit) by
# the cell area
cell_surface = np.pi * simul["cell radius"]**2
max_current_density = simul["cell max current"]/cell_surface

pixel_surface = l**2/(N**2)

max_current_per_pixel = max_current_density * pixel_surface
cell_radius = simul["cell radius"]

s = np.linspace(-l/2,l/2,N)
s1 = np.linspace(0,l.magnitude,N)
meshX, meshY = np.meshgrid(s,s)
R2 = meshX**2 + meshY**2
R = np.sqrt(R2)

# for spiral scanning
omega = (2*np.pi)/T
omega_dl = omega.magnitude


def get_raster_scan_trajectory():
    # generate a list of scanning points for raster scan
    ystep = (simul["raster_dy"]/l*N).magnitude.astype(int)
    ystep = max(ystep,1)
    border = ystep * 1
    
    vy = np.arange(border, N-border+1, ystep)
    vx = np.arange(border, N-border+1)
    
    tr = []
    for i in itertools.product(vx,vy):
        tr.append(i)
        
    return np.array(tr)
        
    
#    return np.array(scan_point_list).T
    
if scanning_function == raster_scan :
    raster_scan_trajectory = get_raster_scan_trajectory()
    plt.clf()
    plt.plot(raster_scan_trajectory.T[1],raster_scan_trajectory.T[0],'.')
    plt.axis([0,N,0,N])
    
    # define a matrix which represents the intensity at the sample
    # it is twice as big as the simulation
    sigma = simul[ "psf_width"].magnitude    
    temp_l = l.magnitude
    temp_s = np.linspace(-temp_l,temp_l,2*N)
    temp_meshX, temp_meshY = np.meshgrid(temp_s,temp_s)
    temp_R2 = temp_meshX**2 + temp_meshY**2
    
    norm_I_scan = 1/(sigma**2 * 2*np.pi) * np.exp( - temp_R2 / (2*(sigma)**2) )
    I_scan = norm_I_scan*lightpower
    tau_on_map = func_on(I_scan,*opsin_dynamics_parameters)
    tau_on_map[tau_on_map>10000] = 10000

#    plt.clf()
#    plt.imshow(1.0/tau_on_map, interpolation='None')
#    plt.colorbar()
    
    norm_I_scan

#%% PLOT TAU ON FUNCTION
#temp = np.linspace(0,0.1)
#plt.plot(temp,func_on(temp,*opsin_dynamics_parameters))   
#%% MODEL CONSTANTS

# CONSTANTS DEFINING THE STATES
C = 0   # Closed state
O = 1   # Opens state
D = 2   # Desensitized state

# TRANSITION RATES MATRICES
K_off = np.zeros([3,3]) # transition coefficient MATRIX when light is OFF
K_on =  np.zeros([3,3]) # transition coefficient MATRIX when light is ON

K_off = pq.Quantity(K_off,pq.ms**-1)
K_on = pq.Quantity(K_on,pq.ms**-1)


#17.9 # usually called "tauoff" [ms] Exp Physiol. 2011 Jan; 96(1): 19–25. 
tau_off = pq.Quantity(tau_off_dl,'ms')
tau_on = pq.Quantity(func_on(lightpower,*opsin_dynamics_parameters),'ms')
tau_on_dl = tau_on.magnitude

K_off[O,C] = 1.0/tau_off
K_on[O,C] = 1.0/tau_off
K_on[C,O] = 1.0/tau_on

K_on[O,D] = kod_mean
K_off[O,D] = kod_mean
K_on[D,C] = kdc_mean
K_off[D,C] = kdc_mean


#%% SIMULATION


def run_simulation(proc_start,proc_stop):
    
#    A = np.zeros((simul["time steps"], 3, N, N))
#    A[0,0,:,:][R<=cell_radius] = 1
    A = np.zeros((3, N, N))
    A[0,:,:][R<=cell_radius] = 1
    
    def ode_model_generic(y, t, k, pos_x, pos_y):
        dy = np.empty_like(y)
        k[C,O] = scanning_function(t, pos_x, pos_y, pulse_duration, tau_on_dl)
        for i, f in enumerate(y):
            dy[i] = -f * sum(k[i, :]) + np.dot(y, k[:, i])  # short notation for the linear system
    
        return dy
     
    ## scan the surface, point by point
    k = K_on.magnitude
    
    print "0% ||||||||||||||||||||||||||||||||||||||||||| 100%"
    print "   [",
    asterisco = 0
      
    for j in range(proc_start,proc_stop): #scan the 2d matrix
        y, x = j%N , j/N
        
        sonda = int(float(j)/N2*100)
        if sonda%5 == 0 and sonda/5>asterisco :
            print '*' ,
            asterisco += 1
    
        # if outside the cell, just pass
        if R[y,x] > cell_radius:
            pass
        else :
            temp = odeint(ode_model_generic,
                       A[:,y,x], # initial condition
                       timespace_dl,
                       (k, x, y), hmax = dt_dl
                       )
#            A[:, 1, y, x] = temp[:,1]
            B = temp[:,1]*max_current_per_pixel.magnitude
            np.savetxt('simul_data/{:05d}-{:05d}.txt'.format(x,y),
                       B,fmt='%f')
        
    
    print "* ]"
            
#    A = A[:,1,:,:]
#    A_d = A * max_current_per_pixel
#    A = A_d.magnitude

N2 = N**2

#p1 = Process(target=run_simulation, args=(0,N2/2))
#p2 = Process(target=run_simulation, args=(N2/2+1,N2))
#p1.start()
#p2.start()
#p1.join()
#p2.join()


N2 = N**2

procs = [None]*processes_num
for i in range(processes_num):
    proc_start = i*N2/processes_num
    proc_end = (i+1)*N2/processes_num
    print i, proc_start, proc_end
    procs[i] = Process(target=run_simulation, args=(proc_start,proc_end,))

for i in range(processes_num):
    procs[i].start()

for i in range(processes_num):
    procs[i].join()
#%%


def current_plot(t,i,*args,**argv):
    ax = plt.plot(t,i,*args,**argv)
    plt.xlabel(t.dimensionality.latex)
    plt.ylabel(i.dimensionality.latex)
    return ax

def surfaceplot(s,*args):
    global norm
    im1 = plt.imshow(s, cmap='hot', norm = norm, interpolation='none')
    im1.set_extent([0,l.magnitude,0,l.magnitude])
    plt.xlabel(l.dimensionality.latex)
    plt.ylabel(l.dimensionality.latex)
    return im1

#print m

#fig = plt.figure(2)
#plt.subplot(2,2,1)
#surfaceplot(A[0,:,:])
#plt.subplot(2,2,2)
#surfaceplot(A[2,:,:])
#plt.subplot(2,2,3)
#surfaceplot(A[-1,:,:])
#plt.subplot(2,2,4)
#current_plot(timespace_d, A_d[:,N/2,N/2])


#%% REBUILD matrix A

import re, os

def get_A_at_time_index(ti):
    A = np.zeros((N,N))
    #reconstruct the output matrix at time_index ti
    for root, dirs, fname_list in os.walk('simul_data') :
        for fname in fname_list:
            m = re.search(r'(?P<x>\d+)-(?P<y>\d+).txt$',fname)            
            if m:
                x = int(m.groupdict()['x'])
                y = int(m.groupdict()['y'])
                A[y,x] = np.loadtxt('simul_data/'+fname)[ti]
    return A

#print get_A_at_time_index(0).shape

for i,t in enumerate(timespace_dl):
    print i,t
    temp = get_A_at_time_index(i)
    np.savetxt('simul_data/images/{:05d}.txt'.format(i), temp)


#%%
# build current array
print "building current array"
I = pq.Quantity(np.zeros(simul["time steps"]), max_current_per_pixel.units)
for i,ti in enumerate(range(simul["time steps"])):
    I[i] =  pq.Quantity(np.sum(get_A_at_time_index(ti)), max_current_per_pixel.units)

q = np.cumsum(I*dt).rescale(pq.C)

#%%

fig = plt.figure(3)
ax1 = fig.add_subplot(1,1,1)

# im1 = ax1.imshow(s0[:,:,0], interpolation = 'none',norm=norm,
#                  extent = [-Xmax, Xmax, -Xmax, Xmax])

#A = A_d.magnitude

m = 0.1 #np.max(A)
norm = pylab.mpl.colors.Normalize(vmin=0, vmax=m)

plt.subplot(2,2,1)
plt.tight_layout()
im1 = surfaceplot(get_A_at_time_index(0))

ax1.annotate('TEST', xy=(0, 0), xytext=(0, 0),
            xycoords='data',
            arrowprops=dict(facecolor='white', shrink=0.05),
            )

#select only 100 frames in A
if simul["time steps"] > 50:
    frame_step = int( float(simul["time steps"])/50 )
else:
    frame_step = 1

#A_anim = A[1:-1:frame_step]
anim_steps_list = range(0,simul["time steps"],frame_step)

def init():
    im1.set_data(get_A_at_time_index(0))
    return([im1])

                                              
def animate(t):
    im1.set_data(get_A_at_time_index(t))
    return([im1])

anim = animation.FuncAnimation(fig,
                               animate,
                               anim_steps_list,
                               interval=10,
                               init_func=init,
                               repeat=True)


rest_v = simul["resting potential"]
#m_thick = simul["cell membrane thinckness"]
spike_threshold = simul["spike potential threshold"]

cell_radius = pq.Quantity(15, pq.um)
cell_surface = 4 * np.pi * cell_radius**2

cell_capacitance_density = pq.Quantity(10**-2, pq.F*pq.m**-2)
cell_capacitance = cell_capacitance_density * cell_surface


# ELECTRIC FIELD
#epsilon0 = pq.electric_constant # *8*10**-12
#ke = 1.0/(np.pi*4*(epsilon0))
#E =  ke * q / (m_thick**2)

# ELECTRIC POTIENTIAL
#dv = rest_v + np.cumsum(E * m_thick)
# spherical capacirot
#dv = rest_v + np.cumsum(q*ke * abs(1/cell_radius - 1/(cell_radius+m_thick)))

dv = rest_v + q/cell_capacitance
dv = dv.rescale('mV')

#spike_time = timespace(np.find(abs()))
spike_index = abs(dv - spike_threshold).argmin()
spike_time = timespace.magnitude[spike_index]
print "Spike time :", spike_time

plt.subplot(2,2,2)
# READ DATA from Marta's experiment
import csv
xdata = []
ydata = []
with open('../Ipython Notebooks (unified notation)/marta_p25.txt', 'rb') as csvfile:
    dialect = csv.Sniffer().sniff(csvfile.read(1024))
    csvfile.seek(0)
    reader = csv.reader(csvfile, dialect)
    for row in reader:
        xdata.append(row[0])
        ydata.append(row[1])

xdata = np.array(xdata)
ydata = np.array(ydata)
plt.plot(xdata,ydata)

current_plot(timespace,I,'r--',lw=2)
plt.title("total current")

plt.xlim(timespace_dl[0], timespace_dl[-1])

plt.tight_layout()




plt.subplot(2,2,3)
current_plot(timespace,q,'-')
plt.title("total charge")
plt.tight_layout()

plt.subplot(2,2,4)
current_plot(timespace[:spike_index+1],dv[:spike_index+1],'-')
plt.plot((spike_time,spike_time),(-70,np.max(dv.magnitude)),'k--')
#current_plot(timespace_d[spike_index+1:],dv[spike_index+1:],'--')
plt.title("membrane potential")
plt.tight_layout()


#%%

#def save_current_stauts(filename):
#    global A_d, timespace_d, simul, dt_d
#    topickle = {
#        "timespace" : timespace,
#        "current matrix" : A,
#        "simulation patameters": simul,
#        "dt" : dt
#    }
#    
#    with open(filename,'wb') as f:
#        pickle.dump(topickle, f)
#    
#def load_status(filename):
#    global A_d, timespace_d, simul, dt_d
#    
#    with open (filename,'rb') as f:
#        tounpickle = pickle.load(f)    
#        timespace = tounpickle["timespace"]
#        A = tounpickle["current matrix"]
#        simul = tounpickle["simulation patameters"]
#        dt = tounpickle["dt"]
    
    
#%%
    
#save_current_stauts("uniform.pkl")
#save_current_stauts("raster.pkl")
#save_current_stauts("spiral.pkl")
#save_current_stauts("spiral_cspeed.pkl")
#save_current_stauts("raster_slow_10s.pkl")

#load_status("./uniform.pkl")
#load_status("./raster.pkl")
#load_status("./spiral.pkl")
#load_status("./spiral_cspeed.pkl")

#%%


#def test(t):
#        omega = 2*np.pi/100
#        th = t*omega
#        r = t/1000/2
#        center = (r*np.cos(th),r*np.sin(th))
#        
#        return r*np.cos(th), r*np.sin(th)
#
#
#def test_cs(t):
#        r = t/1000/2        
#        omega = 2*np.pi/r
#        th = omega*t
#        
#        
#        return r*np.cos(th), r*np.sin(th)
#        
##print  test(timespace)
#
#tt = np.linspace(0,1000,1000)
#
#plt.subplot(1,2,1)
#plt.plot(*test(tt))
#plt.axis('equal')
#
#plt.subplot(1,2,2)
#plt.plot(*test_cs(tt))
#plt.axis('equal')

#%%
#from scipy.integrate import odeint
#import csv
#
#plt.clf()
#
## READ DATA from Marta's experiment
#
#xdata = []
#ydata = []
#with open('../Ipython Notebooks (unified notation)/marta_p25.txt', 'rb') as csvfile:
#    dialect = csv.Sniffer().sniff(csvfile.read(1024))
#    csvfile.seek(0)
#    reader = csv.reader(csvfile, dialect)
#    for row in reader:
#        xdata.append(row[0])
#        ydata.append(row[1])
#
#xdata = np.array(xdata)
#ydata = np.array(ydata)
#
##plt.plot(xdata,ydata)
#
#initial_condition = [simul["cell max current"].magnitude,0,0]
#pulse = mt.Pulse(0,500,10000)
#v = odeint(mt.ode_model_generic, initial_condition, timespace_dl,
#           (K_off.magnitude,K_on.magnitude,pulse), hmax=10)[:,1]
#
##plt.plot(xdata, v ,'b--',linewidth='3',label='fit')
#         
##current_plot(timespace,I,'r-',lw=2)
